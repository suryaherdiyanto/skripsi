<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Pembelian - {{ $purchase->purchase_no }}</title>
    <style>
         h1 {
            font-size: 32px;
            margin: 0;
        }
        .container {
            width: 100%;
            padding: 15px;
        }

        .row {
            width: 100%;
            display: flex;
            margin-bottom: 15px;
        }

        .main-logo {
            max-width: 280px;
            margin-top: 10px;
        }

        .heading {
            text-align: center;
        }

        .heading p {
            margin: 5px 0px;
        }

        .header {
            width: 100%;
        }

        .tb-heading td {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <table class="header">
                <tr>
                    <td style="width: 100%">
                        <div class="heading">
                            <h1>PT. Marga Utama Mandiri</h1>
                            <p>Jl. Nangka - Graha Permai No.1, Dangin Puri Kaja, Denpasar Utara</p>
                            <p>Telp: 0361-432980 &nbsp; &nbsp; E-mail: info@margautamamandiri.com</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 100%">
                        <img class="main-logo" src="{{ public_path('/images/logo-marga.png') }}" alt="Logo Marga" srcset="">
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Laporan Pembelian - {{ $purchase->purchase_no }}</h2>
        </div>
        <div class="row">
            <table cellpadding="5" width="100%">
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Nomor Pembelian</td>
                                <td>{{ $purchase->purchase_no }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Tanggal Pembelian</td>
                                <td>{{ $purchase->date->format('j F Y') }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Status Pembayaran</td>
                                <td>{{ $purchase->getStatusText() }}</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Total Pembelian</td>
                                <td>{{ $purchase->totalFormat() }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Total Dibayar</td>
                                <td>{{ $purchase->totalPaidFormat() }}</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Sisa Saldo</td>
                                <td>{{ $purchase->remainBalanceFormat() }}</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Daftar Entry Barang</h2>
        </div>
        <div class="row">
            <table width="100%" cellpadding="5">
                <thead>
                    <tr class="tb-heading">
                        <td>Nama Barang</td>
                        <td>QTY</td>
                        <td>Harga</td>
                        <td>Total</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($purchase->details as $detail)
                        <tr>
                            <td>{{ $detail->item->name }}</td>
                            <td>{{ $detail->qty }}</td>
                            <td>{{ $detail->item->formatPrice('purchase_price') }}</td>
                            <td>{{ $detail->totalPurchaseFormat() }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" style="text-align: right">Subtotal</td>
                        <td>{{ $purchase->totalFormat() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Daftar Pembayaran</h2>
        </div>
        <div class="row">
            <table width="100%" cellpadding="5">
                <thead>
                    <tr>
                        <td>No Pembayaran</td>
                        <td>Tanggal</td>
                        <td>Total</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($purchase->payments as $payment)
                        <tr>
                            <td>{{ $payment->payment_no }}</td>
                            <td>{{ $payment->date->format('j F Y') }}</td>
                            <td>{{ $payment->totalFormat() }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2" style="text-align: right">Subtotal</td>
                        <td>{{ $purchase->totalPaidFormat() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <table width="100%">
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Mengetahui</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">PT. Marga Utama Mandiri</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">I Made Suastika,SE</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Direktur</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>