<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Pembayaran</title>
    <style>
         h1 {
            font-size: 32px;
            margin: 0;
        }
        .container {
            width: 100%;
            padding: 15px;
        }

        .row {
            width: 100%;
            display: flex;
            margin-bottom: 15px;
        }

        .main-logo {
            max-width: 280px;
            margin-top: 10px;
        }

        .heading {
            text-align: center;
        }

        .heading p {
            margin: 5px 0px;
        }

        .header {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <table class="header">
                <tr>
                    <td style="width: 100%">
                        <div class="heading">
                            <h1>PT. Marga Utama Mandiri</h1>
                            <p>Jl. Nangka - Graha Permai No.1, Dangin Puri Kaja, Denpasar Utara</p>
                            <p>Telp: 0361-432980 &nbsp; &nbsp; E-mail: info@margautamamandiri.com</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 100%">
                        <img class="main-logo" src="{{ public_path('/images/logo-marga.png') }}" alt="Logo Marga" srcset="">
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2 style="margin: 0px">Laporan Pembelian</h2>
            <strong>Periode {{ $start_date->format('j F Y') }} - {{ $end_date->format('j F Y') }}</strong>
        </div>
        <div class="row">
            <table cellpadding="5" border="1" width="100%">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>No Pembayaran</td>
                        <td>No Proyek</td>
                        <td>Tanggal</td>
                        <td>Total Pembayaran</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $payment->payment_no }}</td>
                            <td>{{ $payment->project_no }}</td>
                            <td>{{ $payment->date }}</td>
                            <td>{{ $payment->total }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="row">
            <table width="100%">
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Mengetahui</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">PT. Marga Utama Mandiri</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">I Made Suastika,SE</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Direktur</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>