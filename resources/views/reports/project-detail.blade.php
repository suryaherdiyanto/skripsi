<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Proyek - {{ $project->project_no }}</title>
    <style>
         h1 {
            font-size: 32px;
            margin: 0;
        }
        .container {
            width: 100%;
            padding: 15px;
        }

        .row {
            width: 100%;
            display: flex;
            margin-bottom: 15px;
        }

        .main-logo {
            max-width: 280px;
            margin-top: 10px;
        }

        .heading {
            text-align: center;
        }

        .heading p {
            margin: 5px 0px;
        }

        .header {
            width: 100%;
        }

        .tb-heading td {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <table class="header">
                <tr>
                    <td style="width: 100%">
                        <div class="heading">
                            <h1>PT. Marga Utama Mandiri</h1>
                            <p>Jl. Nangka - Graha Permai No.1, Dangin Puri Kaja, Denpasar Utara</p>
                            <p>Telp: 0361-432980 &nbsp; &nbsp; E-mail: info@margautamamandiri.com</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 100%">
                        <img class="main-logo" src="{{ public_path('/images/logo-marga.png') }}" alt="Logo Marga" srcset="">
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Laporan Proyek - {{ $project->project_no }}</h2>
        </div>
        <div class="row">
            <table cellpadding="5" width="100%">
                <tr>
                    <td>
                        <table class="entry-data">
                            <tr>
                                <td style="font-weight: bold; width: 35%">Nomor Proyek</td>
                            </tr>
                            <tr>
                                <td>{{ $project->project_no }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Nama Proyek</td>
                            </tr>
                            <tr>
                                <td>{{ $project->name }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Tanggal Pengerjaan</td>
                            </tr>
                            <tr>
                                <td>{{ $project->start_date->format('j F Y') }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Customer</td>
                            </tr>
                            <tr>
                                <td>{{ $project->customer->name }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Deskripsi</td>
                            </tr>
                            <tr>
                                <td>{{ $project->description }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Status Pembayaran</td>
                            </tr>
                            <tr>
                                <td>{{ $project->getPaymentStatusText() }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table class="entry-data">
                            <tr>
                                <td style="font-weight: bold; width: 35%">Total Biaya</td>
                            </tr>
                            <tr>
                                <td>{{ $project->totalCostFormat() }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Pemasukan</td>
                            </tr>
                            <tr>
                                <td>{{ $project->totalPaidFormat() }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Pengeluaran</td>
                            </tr>
                            <tr>
                                <td>{{ 'Rp. '. $total_purchases }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Sisa Saldo Pembelian</td>
                            </tr>
                            <tr>
                                <td>{{ 'Rp. '.$remain_purchase }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Sisa Saldo Pembayaran</td>
                            </tr>
                            <tr>
                                <td>{{ $project->remainPaymentFormat() }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="font-weight: bold; width: 35%">Keuntungan</td>
                            </tr>
                            <tr>
                                <td>{{ $project->profitAndLoss()['profit'] }}</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Daftar Pembelian</h2>
        </div>
        <div class="row">
            <table width="100%" cellpadding="5">
                <thead>
                    <tr class="tb-heading">
                        <td>Nomor Pembelian</td>
                        <td>Tanggal</td>
                        <td>Status Pembelian</td>
                        <td>Total Dibayar</td>
                        <td>Total Pembelian</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($project->purchases as $purchase)
                        <tr>
                            <td>{{ $purchase->purchase_no }}</td>
                            <td>{{ $purchase->date->format('j F Y') }}</td>
                            <td>{{ $purchase->getStatusText() }}</td>
                            <td>{{ $purchase->totalPaidFormat() }}</td>
                            <td>{{ $purchase->totalFormat() }}</td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" style="text-align: right">Subtotal</td>
                        <td>{{ $total_purchases }}</td>
                        <td>{{ $project->totalCostFormat() }}</td>
                        <td>Sisa : {{ $remain_purchase }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2>Daftar Pembayaran</h2>
        </div>
        <div class="row">
            <table width="100%" cellpadding="5">
                <thead>
                    <tr>
                        <td>No Pembayaran</td>
                        <td>Tanggal</td>
                        <td>Total</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($project->payments as $payment)
                        <tr>
                            <td>{{ $payment->payment_no }}</td>
                            <td>{{ $payment->date->format('j F Y') }}</td>
                            <td>{{ $payment->totalFormat() }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2" style="text-align: right">Subtotal</td>
                        <td>{{ $project->totalPaidFormat() }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            <table width="100%">
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Mengetahui</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">PT. Marga Utama Mandiri</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">I Made Suastika,SE</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Direktur</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>