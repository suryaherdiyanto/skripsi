<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Laporan Pembelian</title>
    <style>
         h1 {
            font-size: 32px;
            margin: 0;
        }
        .container {
            width: 100%;
            padding: 15px;
        }

        .row {
            width: 100%;
            display: flex;
            margin-bottom: 15px;
        }

        .main-logo {
            max-width: 280px;
            margin-top: 10px;
        }

        .heading {
            text-align: center;
        }

        .heading p {
            margin: 5px 0px;
        }

        .header {
            width: 100%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <table class="header">
                <tr>
                    <td style="width: 100%">
                        <div class="heading">
                            <h1>PT. Marga Utama Mandiri</h1>
                            <p>Jl. Nangka - Graha Permai No.1, Dangin Puri Kaja, Denpasar Utara</p>
                            <p>Telp: 0361-432980 &nbsp; &nbsp; E-mail: info@margautamamandiri.com</p>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; width: 100%">
                        <img class="main-logo" src="{{ public_path('/images/logo-marga.png') }}" alt="Logo Marga" srcset="">
                    </td>
                </tr>
            </table>
        </div>

        <hr>

        <div style="text-align: center; margin-bottom: 15px">
            <h2 style="margin: 0px">Laporan Pembelian</h2>
            <strong>Periode {{ $start_date->format('j F Y') }} - {{ $end_date->format('j F Y') }}</strong>
        </div>
        <div class="row">
            <table border="1" cellpadding="5" width="100%">

                <thead>
                    <tr>
                        <td>No</td>
                        <td>Kode Pembelian</td>
                        <td>Kode Proyek</td>
                        <td>Total Pembelian</td>
                        <td>Total Dibayar</td>
                        <td>Sisa Saldo</td>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($purchases as $purchase)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $purchase->purchase_no }}</td>
                            <td>{{ $purchase->project_no }}</td>
                            <td>{{ $purchase->total }}</td>
                            <td>{{ $purchase->total_paid }}</td>
                            <td>{{ $purchase->remain_balance }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3" style="text-align: right;">Total</td>
                        <td>{{ $datasubtotal->paid_subtotal }}</td>
                        <td>{{ $datasubtotal->remain_balance_subtotal }}</td>
                        <td>{{ $datasubtotal->subtotal }}</td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="row">
            <table width="100%">
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Mengetahui</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">PT. Marga Utama Mandiri</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">I Made Suastika,SE</td>
                </tr>
                <tr>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td width="16.6%">&nbsp;</td>
                    <td colspan="2">Direktur</td>
                </tr>
            </table>
        </div>
    </div>
</body>
</html>