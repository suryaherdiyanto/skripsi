export default {
    methods: {
        getAuthProfile() {
            if (this.$store.state.auth.token) {
                axios.get(this.$store.getters.meUrl, {
                    headers: {
                        'Authorization': `Bearer ${this.$store.state.auth.token}`
                    }
                })
                .then((response) => {

                    this.$store.commit('setUser', response.data.data);
                    this.$store.commit('setLoggedIn');

                    this.$store.dispatch('getPermissions').then((response) => {
                        this.$store.commit('setPermissions', response.data);
                    }).catch((error) => {
                        console.log(error);
                        
                    });
                    
                })
                .catch((error) => {
                    
                    if (error.response.status == 401) {
                        this.logoutUser();
                    } else {
                        this.errorMessage(error);
                    }

                })
            }
        },

        logoutUser() {
            this.$store.commit('setUser', null);
            this.$store.commit('setToken', false);
            this.$store.commit('clearPermissions');
            this.$store.commit('setLoggedOut');
            localStorage.removeItem('userToken');
            this.pageTo('login');
        },

        loginUser(response) {
            this.$store.commit('processed');
            this.$store.commit('setLoggedIn');
            this.$store.commit('setToken', response.access_token)
            localStorage.setItem('userToken', response.access_token);
            this.$router.push({ name: 'admin.dashboard' });
        },

        pageTo(name, params = {}) {
            this.$router.push({ name: name, params: params, query: params.query });
        },

        hasPermission(permission) {
            if (this.$store.state.auth.permissions.some((item) => item === permission)) {
                return true;
            }

            return false;
        },

        setFocus(elem) {
            elem.focus();
        },

        clearForm(form) {
            let keys = Object.keys(form);

            keys.forEach((item) => {
                
                if (typeof(form[item]) === 'string') {
                    form[item] = '';
                } else if(typeof(form[item]) === 'object') {
                    if (Array.isArray(form[item])) {
                        form[item] = [];
                    } else {
                        form[item] = {};
                    }
                } else {
                    form[item] = 0;
                }
                
            });
        },

        setDelay(timeout) {
            return new Promise((resolve) => {
                setTimeout(resolve, timeout);
            });
        },

        confirmBox(message = 'Hapus data ini?') {
            return this.$confirm(message, 'Konfirmasi', {
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                type: 'warning'
            });
        },

        errorMessage(error) {
            this.$message({
                message: error.response.data.message,
                type: 'error'
            });
        },

        now(separator = '-') {
            let date = new Date();
    
            return `${date.getFullYear()}${separator}${date.getMonth()+1}${separator}${date.getDate()}`;
        },

        fillForm(form, data) {
            const keys = Object.keys(form);

            form.id = data.id;
            keys.forEach((item) => {
                form[item] = data[item]
            });
        },
    }
}