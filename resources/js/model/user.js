export const fetchUser = async function(route, token, {s, show, page, trashed}) {
    const { data } = await axios.get(`${route}?page=${page}&q=${s}&perpage=${show}&trashed=${trashed}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const getUser = async function(route, token) {
    const { data } = await axios.get(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const createUser = async function(route, token, formData) {
    const { data } = await axios.post(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const updateUser = async function(route, token, formData) {
    const { data } = await axios.put(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const deleteUser = async function(route, token) {
    const { data } = await axios.delete(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const restoreUser = async function(route, token) {
    const { data } = await axios.put(route, {}, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}