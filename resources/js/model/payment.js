export const fetchPayment = async function(route, token, {s, show, page, trashed}) {
    const { data } = await axios.get(`${route}?page=${page}&q=${s}&perpage=${show}&trashed=${trashed}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const getPayment = async function(route, token) {
    const { data } = await axios.get(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const createPayment = async function(route, token, formData) {
    const { data } = await axios.post(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const updatePayment = async function(route, token, formData) {
    const { data } = await axios.put(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const deletePayment = async function(route, token) {
    const { data } = await axios.delete(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const restorePayment = async function(route, token) {
    const { data } = await axios.put(route, {}, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}