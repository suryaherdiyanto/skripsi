export const fetchProject = async function(route, token, {s, show, page, trashed}) {
    const { data } = await axios.get(`${route}?include=customer&page=${page}&q=${s}&perpage=${show}&trashed=${trashed}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const getProject = async function(route, token) {
    const { data } = await axios.get(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const createProject = async function(route, token, formData) {
    const { data } = await axios.post(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const updateProject = async function(route, token, formData) {
    const { data } = await axios.put(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const deleteProject = async function(route, token) {
    const { data } = await axios.delete(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const restoreProject = async function(route, token) {
    const { data } = await axios.put(route, {}, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}