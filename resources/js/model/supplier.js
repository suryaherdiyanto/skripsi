export const fetchSupplier = async function(route, token, {s, show, page, trashed}) {
    const { data } = await axios.get(`${route}?page=${page}&q=${s}&perpage=${show}&trashed=${trashed}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const fetchAllSupplier = async function(route, token) {
    const { data } = await axios.get(`${route}?all=1`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const getSupplier = async function(route, token) {
    const { data } = await axios.get(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const createSupplier = async function(route, token, formData) {
    const { data } = await axios.post(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const updateSupplier = async function(route, token, formData) {
    const { data } = await axios.put(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const deleteSupplier = async function(route, token) {
    const { data } = await axios.delete(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const restoreSupplier = async function(route, token) {
    const { data } = await axios.put(route, {}, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}