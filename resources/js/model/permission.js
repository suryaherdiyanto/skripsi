export const fetchAllPermission = async function(route, token) {
    const { data } = await axios.get(`${route}?all=1`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}