export const fetchRole = async function(route, token, {s, show, page, trashed}) {
    const { data } = await axios.get(`${route}?page=${page}&q=${s}&perpage=${show}&trashed=${trashed}`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const fetchAllRole = async function(route, token) {
    const { data } = await axios.get(`${route}?all=1`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const getRole = async function(route, token) {
    const { data } = await axios.get(`${route}?include=permissions`, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const createRole = async function(route, token, formData) {
    const { data } = await axios.post(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const updateRole = async function(route, token, formData) {
    const { data } = await axios.put(route, formData, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const deleteRole = async function(route, token) {
    const { data } = await axios.delete(route, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}

export const restoreRole = async function(route, token) {
    const { data } = await axios.put(route, {}, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    return data;
}