export default {
    
    loginUrl: (state) => {
        return state.endpoint + state.api.login;
    },

    logoutUrl: (state) => {
        return state.endpoint + state.api.logout;
    },

    meUrl: (state) => {
        return state.endpoint + state.api.me;
    },

    permissionsUrl: (state) => {
        return state.endpoint + state.api.me + '/permission';
    },

    userUrl: (state) => {
        return state.endpoint + state.api.user.index;
    },

    roleUrl: (state) => {
        return state.endpoint + state.api.role.index;
    },

    permissionUrl: (state) => {
        return state.endpoint + state.api.permission.index;
    },

    supplierUrl: (state) => {
        return state.endpoint + state.api.supplier.index;
    },
    customerUrl: (state) => {
        return state.endpoint + state.api.customer.index;
    },
    projectUrl: (state) => {
        return state.endpoint + state.api.project.index;
    },
    projectPurchaseUrl: (state) => (projectId) => {
        return `${state.endpoint}${state.api.project.index}/${projectId}${state.api.project.purchases}`;
    },
    projectPaymentUrl: (state) => (projectId) => {
        return `${state.endpoint}${state.api.project.index}/${projectId}${state.api.project.payments}`;
    },
    paymentClearTrashUrl: (state) => {
        return `${state.endpoint}${state.api.project.index}${state.api.project.payments}/trash/clear`;
    },
    paymentUrl: (state) => (paymentId) => {
        return `${state.endpoint}${state.api.project.index}${state.api.project.payments}/${paymentId}`;
    },
    purchaseUrl: (state) => (purchaseId) => {
        return `${state.endpoint}${state.api.project.index}${state.api.project.purchases}/${purchaseId}`;
    },
    purchaseClearTrashUrl: (state) => {
        return `${state.endpoint}${state.api.project.index}${state.api.project.purchases}/trash/clear`;
    },
    purchasePurchaseDetailUrl: (state) => (purchaseId) => {
        return `${state.endpoint}${state.api.purchase.index}/${purchaseId}/details`;
    },
    purchaseDetailUrl: (state) => (purchaseDetailId) => {
        return `${state.endpoint}${state.api.purchase.index}${state.api.purchase.details}/${purchaseDetailId}`;
    },
    purchaseDetailClearTrashUrl: (state) => {
        return `${state.endpoint}${state.api.purchase.index}${state.api.purchase.details}/trash/clear`;
    },
    purchasePurchasePaymentUrl: (state) => (purchaseId) => {
        return `${state.endpoint}${state.api.purchase.index}/${purchaseId}${state.api.purchase.payments}`;
    },
    purchasePaymentClearTrashUrl: (state) => {
        return `${state.endpoint}${state.api.purchase.index}${state.api.purchase.payments}/trash/clear`;
    },
    purchasePaymentUrl: (state) => (purchasePaymentId) => {
        return `${state.endpoint}${state.api.purchase.index}${state.api.purchase.payments}/${purchasePaymentId}`;
    },
    itemUrl: (state) => {
        return state.endpoint + state.api.item.index;
    },
    authHeader: (state) => {
        return {'Authorization': `Bearer ${state.auth.token}`}
    },
    reportUrl: (state) => (type) => {
        return state.endpoint + '/reports' + state.api.report[type];
    },
    reportTotalUrl: (state) => {
        return state.endpoint + '/reports/totals';
    },
    resetPasswordUrl: (state) => {
        return state.endpoint + '/me/reset-password';
    }

}