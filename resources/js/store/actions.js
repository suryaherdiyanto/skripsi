export default {
    async login({ getters }, { email, password }) {
        let { data } = await axios.post(getters.loginUrl, {
            email: email,
            password: password
        });

        return data;
    },

    async logout({ state, getters }) {
        let { data } = await axios.post(getters.logoutUrl, {}, {
            headers: {
                'Authorization': `Bearer ${state.auth.token}`
            }
        });

        return data;
    },

    async getPermissions({ state, getters }) {
        let { data } = await axios.get(getters.permissionsUrl, {
            headers: {
                'Authorization': `Bearer ${state.auth.token}`
            }
        });

        return data;
    },

    refreshProject({ commit, state, getters }, projectId) {
        commit('clearProjectMeta');
        axios.get(`${getters.projectUrl}/${projectId}?include=customer`, {
            headers: {
                'Authorization': `Bearer ${state.auth.token}`
            }
        }).then((response) => {
            setTimeout(() => {
                let data = response.data.data;

                Object.assign(data, response.data.meta);
                commit('setProjectMeta', Object.assign(data));
            }, 100);
        });
    },

    async refreshPurchase({ commit, state, getters }, purchaseId) {
        commit('clearPurchaseMeta');
        let { data } = await axios.get(`${getters.purchaseUrl(purchaseId)}`, {
            headers: {
                'Authorization': `Bearer ${state.auth.token}`
            }
        });

        return data;
    }
}