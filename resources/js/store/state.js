export default {
    endpoint: '/api',
    buttonState: false,
    loadingState: false,
    passwordResetDialog: false,
    errors: {},
    dataShow: [10, 20, 50],
    pageTitle: '',
    api: {
        login: '/login',
        logout: '/logout',
        me: '/me',
        user: {
            index: '/users',
        },
        role: {
            index: '/roles'
        },
        permission: {
            index: '/permissions'
        },
        supplier: {
            index: '/suppliers'
        },
        customer: {
            index: '/customers'
        },
        item: {
            index: '/items'
        },
        project: {
            index: '/projects',
            purchases: '/purchases',
            payments: '/payments'
        },
        purchase: {
            index: '/purchase',
            details: '/details',
            payments: '/payments'
        },
        report: {
            dashboard: '/dashboard',
            purchase: '/purchase',
            payment: '/payment',
            project: '/project',
            profit: '/profit'
        }
    },
    auth: {
        isLoggedIn: false,
        token: (localStorage.getItem('userToken')) ? localStorage.getItem('userToken') : false,
        loggedinUser: {
            name: '',
            role: {
                data: {}
            }
        },
        permissions: []
    },
    meta: {
        project: {
            profit_loss: {},
            customer: {
                data: {}
            }
        },
        purchase: {}
    }
}