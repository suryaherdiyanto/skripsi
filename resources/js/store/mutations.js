export default {
    loading(state) {
        state.loadingState = true;
    },

    processing(state) {
        state.buttonState = true;
    },

    loaded(state) {
        state.loadingState = false;
    },

    processed(state) {
        state.buttonState = false;
    },

    fillError(state, errors) {
        state.errors = errors;
    },

    clearError(state) {
        state.errors = {}
    },

    setUser(state, user) {
        state.auth.loggedinUser = user;
    },

    setToken(state, token) {
        state.auth.token = token;
    },

    removeUser(state) {
        state.auth.loggedinUser = {
            role: {
                data: {}
            }
        }
    },

    removeToken(state) {
        state.auth.token = '';
    },

    changePageTitle(state, title) {
        state.pageTitle = title
    },

    setLoggedIn(state) {
        state.auth.isLoggedIn = true;
    },

    setLoggedOut(state) {
        state.auth.isLoggedIn = false;
    },

    setPermissions(state, permissions) {
        state.auth.permissions.push(...permissions.map((item) => item.name));
    },

    clearPermissions(state) {
        state.auth.permissions = [];
    },

    setProjectMeta(state, data) {
        state.meta.project = data;
    },

    clearProjectMeta(state) {
        state.meta.project = {
            customer: {
                data: {}
            }
        }
    },

    setPurchaseMeta(state, data) {
        state.meta.purchase = data;
    },

    clearPurchaseMeta(state) {
        state.meta.purchase = {};
    },

    addProjectTotalCost(state, nominal) {
        state.meta.project.total_cost += parseFloat(nominal);
    },

    subProjectTotalCost(state, nominal) {
        state.meta.project.total_cost -= parseFloat(nominal);
    },

    addProjectTotalPaid(state, nominal) {
        state.meta.project.total_paid += parseFloat(nominal);
    },

    subProjectTotalPaid(state, nominal) {
        state.meta.project.total_paid -= parseFloat(nominal);
    },

    addPurchaseTotalPaid(state, nominal) {
        state.meta.purchase.total_paid += parseFloat(nominal);
    },

    subPurchaseTotalPaid(state, nominal) {
        state.meta.purchase.total_paid -= parseFloat(nominal);
    },

    updateProjectPaymentStatus(state, status) {
        state.meta.project.payment_status = status;
    },

    showPasswordDialog(state) {
        state.passwordResetDialog = true;
    },

    hidePasswordDialog(state) {
        state.passwordResetDialog = false;
    }

}