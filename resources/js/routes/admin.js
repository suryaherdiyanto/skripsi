import Login from '../pages/Login.vue';
import Admin from '../pages/Admin.vue';
import Dashboard from '../pages/Dashboard.vue';
import User from '../pages/User/Index.vue';
import UserForm from '../pages/User/UserForm.vue';
import Role from '../pages/Role/Index.vue';
import RoleForm from '../pages/Role/RoleForm.vue';
import Supplier from '../pages/Supplier/Index.vue';
import SupplierForm from '../pages/Supplier/SupplierForm.vue';
import Customer from '../pages/Customer/Index.vue';
import CustomerForm from '../pages/Customer/CustomerForm.vue';
import Item from '../pages/Item/Index.vue';
import ItemForm from '../pages/Item/ItemForm.vue';
import Project from '../pages/Project/Index.vue';
import ProjectForm from '../pages/Project/ProjectForm.vue';
import ProjectReport from '../pages/Project/ProjectReport.vue';
import ProjectDetail from '../pages/Project/ProjectDetail.vue';
import PurchaseForm from '../pages/Project/detail/purchase/PurchaseForm.vue';
import ListDetail from '../pages/Project/detail/ListDetail.vue';
import PaymentForm from '../pages/Project/detail/payment/PaymentForm.vue';
import PurchaseDetail from '../pages/Purchase/PurchaseDetail.vue';
import PurchaseDetailForm from '../pages/Purchase/PurchaseDetailForm.vue';
import PurchaseDetailList from '../pages/Purchase/DetailList.vue';
import PurchaseDetailEditForm from '../pages/Purchase/PurchaseDetailEditForm.vue';
import PurchasePaymentForm from '../pages/Purchase/payment/PaymentForm.vue';
import ProjectReportDetail from '../pages/Project/ProjectReportDetail.vue';
import PurchaseReport from '../pages/Purchase/PurchaseReport.vue';
import PurchaseReportDetail from '../pages/Purchase/PurchaseReportDetail.vue';
import PaymentReport from '../pages/Project/PaymentReport.vue';



export default [
    {
        path: '/',
        name: 'login',
        component: Login
    },

    {
        path: '/admin',
        name: 'admin',
        component: Admin,
        children: [
            {
                path: 'dashboard',
                name: 'admin.dashboard',
                component: Dashboard
            },
            {
                path: 'user',
                name: 'admin.user',
                component: User
            },
            {
                path: 'user/create',
                name: 'admin.user.create',
                component: UserForm
            },
            {
                path: 'role',
                name: 'admin.role',
                component: Role
            },
            {
                path: 'role/create',
                name: 'admin.role.create',
                component: RoleForm
            },
            {
                path: 'role/edit/:id',
                name: 'admin.role.edit',
                component: RoleForm,
                props: {
                    action: 'edit'
                }
            },
            {
                path: 'supplier',
                name: 'admin.supplier',
                component: Supplier
            },
            {
                path: 'supplier/create',
                name: 'admin.supplier.create',
                component: SupplierForm
            },
            {
                path: 'supplier/:id/edit',
                name: 'admin.supplier.edit',
                component: SupplierForm,
                props: {
                    action: 'edit'
                }
            },
            {
                path: 'customer',
                name: 'admin.customer',
                component: Customer
            },
            {
                path: 'customer/create',
                name: 'admin.customer.create',
                component: CustomerForm
            },
            {
                path: 'customer/:id/edit',
                name: 'admin.customer.edit',
                component: CustomerForm,
                props: {
                    action: 'edit'
                }
            },
            {
                path: 'item',
                name: 'admin.item',
                component: Item
            },
            {
                path: 'item/create',
                name: 'admin.item.create',
                component: ItemForm
            },
            {
                path: 'item/:id/edit',
                name: 'admin.item.edit',
                component: ItemForm,
                props: {
                    action: 'edit'
                }
            },
            {
                path: 'project',
                name: 'admin.project',
                component: Project
            },
            {
                path: 'project/create',
                name: 'admin.project.create',
                component: ProjectForm
            },
            {
                path: 'project/report',
                name: 'admin.project.report',
                component: ProjectReport
            },
            {
                path: 'purchase/report',
                name: 'admin.purchase.report',
                component: PurchaseReport
            },
            {
                path: 'payment/report',
                name: 'admin.payment.report',
                component: PaymentReport
            },
            {
                path: 'project/report/:projectId',
                name: 'admin.project.report.detail',
                component: ProjectReportDetail
            },
            {
                path: 'purchase/report/:purchaseId',
                name: 'admin.purchase.report.detail',
                component: PurchaseReportDetail
            },
            {
                path: 'project/:id/edit',
                name: 'admin.project.edit',
                component: ProjectForm,
                props: {
                    action: 'edit'
                }
            },
            {
                path: 'project/:id',
                name: 'admin.project.detail',
                component: ProjectDetail,
                children: [
                    {
                        path: 'detail',
                        name: 'admin.project.detail.index',
                        component: ListDetail
                    },
                    {
                        path: 'payment/create',
                        name: 'admin.project.payment.create',
                        component: PaymentForm
                    },
                    {
                        path: 'payment/:paymentId/edit',
                        name: 'admin.project.payment.edit',
                        component: PaymentForm,
                        props: {
                            action: 'edit'
                        }
                    },
                    {
                        path: 'purchase/create',
                        name: 'admin.project.purchase.create',
                        component: PurchaseForm
                    },
                    {
                        path: 'purchase/:purchaseId/details',
                        name: 'admin.project.purchase.detail',
                        component: PurchaseDetail,
                        children: [
                            {
                                path: '/',
                                name: 'admin.project.purchase.detail.index',
                                component: PurchaseDetailList,
                                props: {
                                    action: 'create'
                                }
                            },
                            {
                                path: 'create',
                                name: 'admin.project.purchase.detail.create',
                                component: PurchaseDetailForm
                            },
                            {
                                path: 'edit/:purchaseDetailId',
                                name: 'admin.project.purchase.detail.edit',
                                component: PurchaseDetailEditForm
                            },
                            {
                                path: 'payment/create',
                                name: 'admin.project.purchase.detail.payment.create',
                                component: PurchasePaymentForm,
                            },
                            {
                                path: 'payment/:purchasePaymentId/create',
                                name: 'admin.project.purchase.detail.payment.edit',
                                component: PurchasePaymentForm,
                                props: {
                                    action: 'edit'
                                }
                            }
                        ]
                    }
                ]
            }
        ]
    }
]