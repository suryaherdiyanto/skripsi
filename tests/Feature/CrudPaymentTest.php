<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Project;
use App\Payment;

class CrudPaymentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $endpoint = 'api/projects';

    public function testIndex()
    {
        $project = Project::has('payments')->first();
        $response = $this->json('GET', $this->endpoint . '/' . $project->id . '/payments');

        $count = $project->payments->count();

        $response->assertStatus(200)->assertJsonCount($count, 'data');
    }

    public function testCreate()
    {
        $project = factory(Project::class)->create();
        $payment = factory(Payment::class)->make();
        unset($payment['payment_no']);
        unset($payment['project_id']);
        $response = $this->json('POST', $this->endpoint . '/' . $project->id . '/payments', $payment->toArray());

        $response->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseHas('payments', $payment->toArray());
    }

    public function testShow()
    {
        $payment = Payment::all(['id', 'payment_no'])->random();

        $response = $this->json('GET', $this->endpoint . '/payments' . '/' . $payment->id);

        $response->assertStatus(200)->assertJson(['data' => $payment->toArray()]);
    }

    public function testUpdate()
    {
        $payment = Payment::all()->random();

        $response = $this->json('PUT', $this->endpoint . '/payments' . '/' . $payment->id, $payment->toArray());

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
    }

    public function testDelete()
    {
        $project = Project::has('payments')->first();
        $payment = $project->payments()->first();

        $response = $this->json('DELETE', $this->endpoint . '/payments' . '/' . $payment->id);

        $payment = Payment::withTrashed()->find($payment->id);

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
        $this->assertSoftDeleted('payments', $payment->toArray());
    }
}
