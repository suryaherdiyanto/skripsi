<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Project;

class CrudProjectTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $endpoint = 'api/projects';

    public function testIndex()
    {
        $response = $this->json('GET', $this->endpoint);

        $count = Project::paginate(10)->count();

        $response->assertStatus(200)->assertJsonCount($count, 'data');
    }

    public function testCreate()
    {
        $data = factory(Project::class)->make();

        $response = $this->json('POST', $this->endpoint, $data->toArray());


        $response->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseHas('projects', $data->toArray());
    }

    public function testShow()
    {
        $data = Project::all(['id','name'])->random();

        $response = $this->json('GET', $this->endpoint . '/' . $data->id);

        $response->assertStatus(200)->assertJson(['data' => $data->toArray()]);
    }

    public function testUpdate()
    {
        $data = Project::all()->random();
        $response = $this->json('PUT', $this->endpoint . '/' . $data->id, $data->toArray());

        $response->assertStatus(200)->assertJson([
            'status' => 'ok'
        ]);
    }

    public function testDelete()
    {
        $data = Project::first();
        $response = $this->json('DELETE', $this->endpoint . '/' . $data->id);

        $response->assertStatus(200)->assertJson([
            'status' => 'ok'
        ]);

        $data = Project::withTrashed()->find($data->id);

        $this->assertSoftDeleted('projects', $data->toArray());
    }
}
