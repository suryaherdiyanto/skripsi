<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Project;
use App\Purchase;

class CrudPurchasePaymentTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $endpoint = 'api/projects';

    public function testIndex()
    {
        $purchase = Purchase::has('payments')->first();
        $response = $this->json('GET', $this->endpoint . '/' . $purchase->id . '/purchase/payments');

        $count = $purchase->payments->count();

        $response->assertStatus(200)->assertJsonCount($count, 'data');
    }

    public function testCreate()
    {
        $purchase = factory(Purchase::class)->create();
        $payment = factory(PurchasePayment::class)->make();
        unset($payment['payment_no']);
        unset($payment['project_id']);
        $response = $this->json('POST', $this->endpoint . '/' . $purchase->id . '/purchase/payments', $payment->toArray());

        $response->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseHas('payments', $payment->toArray());
    }

    public function testShow()
    {
        $payment = PurchasePayment::all(['id', 'payment_no'])->random();

        $response = $this->json('GET', $this->endpoint . '/purchase' . '/' . $payment->id);

        $response->assertStatus(200)->assertJson(['data' => $payment->toArray()]);
    }

    public function testUpdate()
    {
        $payment = PurchasePayment::all()->random();

        $response = $this->json('PUT', $this->endpoint . '/purchase' . '/' . $payment->id, $payment->toArray());

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
    }

    public function testDelete()
    {
        $purchase = Purchase::has('payments')->first();
        $payment = $purchase->payments()->first();

        $response = $this->json('DELETE', $this->endpoint . '/purchase' . '/' . $payment->id);

        $payment = PurchasePayment::withTrashed()->find($payment->id);

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
        $this->assertSoftDeleted('purchase-payments', $payment->toArray());
    }
}
