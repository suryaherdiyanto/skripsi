<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Project;
use App\Purchase;

class CrudPurchaseTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $endpoint = 'api/projects';

    public function testIndex()
    {
        $project = Project::has('purchases')->first();
        $response = $this->json('GET', $this->endpoint . '/' . $project->id . '/purchases');

        $count = $project->purchases->count();

        $response->assertStatus(200)->assertJsonCount($count, 'data');
    }

    public function testCreate()
    {
        $project = factory(Project::class)->create();
        $purchase = factory(Purchase::class)->make();
        unset($purchase['purchase_no']);
        unset($purchase['project_id']);
        $response = $this->json('POST', $this->endpoint . '/' . $project->id . '/purchases', $purchase->toArray());

        $response->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseHas('purchases', $purchase->toArray());
    }

    public function testShow()
    {
        $purchase = Purchase::all(['id', 'purchase_no'])->random();

        $response = $this->json('GET', $this->endpoint . '/purchases' . '/' . $purchase->id);

        $response->assertStatus(200)->assertJson(['data' => $purchase->toArray()]);
    }

    public function testUpdate()
    {
        $purchase = Purchase::all()->random();

        $response = $this->json('PUT', $this->endpoint . '/purchases' . '/' . $purchase->id, $purchase->toArray());

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
    }

    public function testDelete()
    {
        $project = Project::has('purchases')->first();
        $purchase = $project->purchases()->first();

        $response = $this->json('DELETE', $this->endpoint . '/purchases' . '/' . $purchase->id);

        $purchase = Purchase::withTrashed()->find($purchase->id);

        $response->assertStatus(200)->assertJson(['status' => 'ok']);
        $this->assertSoftDeleted('purchases', $purchase->toArray());
    }
}
