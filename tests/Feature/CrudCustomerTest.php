<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Customer;

class CrudCustomerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    private $endpoint = 'api/customers';

    public function testIndex()
    {
        $response = $this->json('GET', $this->endpoint);

        $count = Customer::paginate(10)->count();

        $response->assertStatus(200)->assertJsonCount($count, 'data');
    }

    public function testCreate()
    {
        $data = factory(Customer::class)->make();

        $response = $this->json('POST', $this->endpoint, $data->toArray());


        $response->assertStatus(201)->assertJson([
            'status' => 'ok'
        ]);

        $this->assertDatabaseHas('customers', $data->toArray());
    }

    public function testShow()
    {
        $data = Customer::all(['id','name'])->random();

        $response = $this->json('GET', $this->endpoint . '/' . $data->id);

        $response->assertStatus(200)->assertJson(['data' => $data->toArray()]);
    }

    public function testUpdate()
    {
        $data = Customer::all()->random();
        $response = $this->json('PUT', $this->endpoint . '/' . $data->id, $data->toArray());

        $response->assertStatus(200)->assertJson([
            'status' => 'ok'
        ]);
    }

    public function testDelete()
    {
        $data = Customer::doesntHave('projects')->first();
        $response = $this->json('DELETE', $this->endpoint . '/' . $data->id);

        $response->assertStatus(200)->assertJson([
            'status' => 'ok'
        ]);

        $data = Customer::withTrashed()->find($data->id);
            
        $this->assertSoftDeleted('customers', $data->toArray());
    }
}
