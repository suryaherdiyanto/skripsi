<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'project_no',
        'start_date',
        'payment_status',
        'total_cost',
        'total_payment',
        'total_paid',
        'description',
        'customer_id',
        'user_id'
    ];

    protected $dates = [
        'start_date'
    ];

    private $paymentStatusText = [
        '0' => 'Belum Dibayar',
        '1' => 'Dibayar Sebagian',
        '2' => 'Lunas'
    ];

    public function getPaymentStatusText()
    {
        return $this->paymentStatusText[$this->attributes['payment_status']];
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function purchases()
    {
        return $this->hasMany(Purchase::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function totalPurchase()
    {
        if ($this->purchases) {
            return $this->purchases->sum('total');
        }

        return 0;
    }

    public function totalPurchasePayment()
    {
        if ($this->purchases) {
            return $this->purchases->sum('total_paid');
        }

        return 0;
    }

    public function profitAndLoss()
    {
        $profit = $this->totalPayment() - $this->totalPurchasePayment();
        $data = [
            'profit' => 'Rp. 0',
            'loss' => 'Rp. 0'
        ];

        if ($profit > 0) {
            $data['profit'] = 'Rp. ' . number_format($profit);
        } else if($profit < 0) {
            $data['loss'] = 'Rp. ' . number_format(abs($profit));
        }

        return $data;
    }

    public function totalBalance()
    {
        return $this->total_payment - $this->total_paid;
    }

    public function totalPayment()
    {
        if ($this->payments) {
            return $this->payments->sum('total');
        }

        return 0;
    }

    public function totalCustomerPay()
    {
        $total = 0;

        if ($this->purchases) {
            foreach ($this->purchases as $purchase) {
                $total += $purchase->countTotalSell();
            }
        }

        return $total;
    }

    public function totalCostFormat()
    {
        return 'Rp. ' . number_format($this->attributes['total_cost'], 0);
    }

    public function totalPaidFormat()
    {
        return 'Rp. ' . number_format($this->attributes['total_paid'], 0);
    }

    public function remainPaymentFormat()
    {
        return 'Rp. ' . number_format($this->remainPayment(), 0);
    }

    public function profitFormat()
    {
        return 'Rp. ' . number_format($this->profit(), 0);
    }

    public function paymentStatusText()
    {
        return $this->paymentStatusText[$this->attributes['payment_status']];
    }

    public function clearPurchases($forceDelete = false)
    {
        if ($this->purchases) {
            foreach ($this->purchases as $purchase) {
                $purchase->delete();
            }
        }
    }

    public function restorePurchases()
    {
        $purchases = $this->purchases()->onlyTrashed()->get();

        if ($purchases->count() > 0) {
            foreach ($purchases as $purchase) {
                $purchase->restore();
            }
        }
    }

    public function restorePayments()
    {
        $payments = $this->payments()->onlyTrashed()->get();

        if ($payments->count() > 0) {
            foreach ($payments as $payment) {
                $payment->restore();
            }
        }
    }

    public function clearPayments($forceDelete = false)
    {
        if ($this->payments) {
            foreach ($this->payments as $payment) {
                $payment->delete();
            }
        }
    }

    public function remainPayment()
    {
        return $this->total_payment - $this->total_paid;
    }
}
