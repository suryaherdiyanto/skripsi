<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'payment_no',
        'date',
        'total',
        'project_id'
    ];

    protected $dates = [
        'date',
        'deleted_at'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function totalFormat()
    {
        return 'Rp. '.number_format($this->total);
    }
}
