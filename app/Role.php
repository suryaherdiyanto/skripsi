<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name'
    ];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permission');
    }

    public function givePermissions($permissions)
    {
        $data = [];
        $permissionData = $this->permissions();
        
        if ($permissions instanceof \Illuminate\Database\Eloquent\Collection) {
            foreach ($permissions as $permission) {
                $data[] = $permission->id;
            }
        } else {
            foreach ($permissions as $key => $permission) {
                $data[] = Permission::firstOrNew(['name' => $permission])->id;
            }
        }
        

        return $permissionData->attach($data);
    }
}
