<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\ValidationRequestException;

class UpdateUserRequest extends FormRequest
{
    use ValidationRequestException;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [
            'required' => 'Bidang ini harus diisi',
            'string' => 'Bidang ini harus berupa string',
            'min' => 'Panjang karakter minimal :min',
            'confirmed' => 'Konfirmasi password tidak sama'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'password' => 'required|string|confirmed|min:6'
        ];
    }
}
