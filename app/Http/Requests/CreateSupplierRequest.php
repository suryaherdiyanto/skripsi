<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Dingo\Api\Exception\StoreResourceFailedException;

class CreateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new StoreResourceFailedException("Terjadi Kesalahan", $validator->errors());
        
    }

    public function all($data = [])
    {
        $data = array_map(function($item) use($data) {
            if (!empty($item)) { 
                return $item; 
            }
        }, parent::all());

        return $data;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:150',
            'email' => 'email|nullable',
            'fax' => 'string|max:30|nullable',
            'phone' => 'required|string|max:20',
            'address' => 'required'
        ];
    }
}
