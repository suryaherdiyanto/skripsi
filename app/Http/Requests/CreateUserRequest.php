<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Dingo\Api\Exception\StoreResourceFailedException;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function failedValidation(Validator $validator)
    {
        throw new StoreResourceFailedException("Terjadi Kesalahan", $validator->errors());
    }

    public function messages()
    {
        return [
            'required' => 'Bidang ini harus diisi',
            'string' => 'Bidang ini harus berupa string',
            'min' => 'Panjang karakter minimal :min',
            'confirmed' => 'Konfirmasi password tidak sama'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|confirmed|min:6'
        ];
    }
}
