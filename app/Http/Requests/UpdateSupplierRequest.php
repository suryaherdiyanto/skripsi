<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Contracts\Validation\Validator;

class UpdateSupplierRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function StoreResourceFailedException(Validator $validator)
    {
        throw new StoreResourceFailedException("Terjadi Kesalahan", $validator->errors());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'email|nullable',
            'fax' => 'string|max:30|nullable',
            'phone' => 'required|string|max:20',
            'address' => 'required'
        ];
    }
}
