<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PaymentTransformer;
use App\Http\Requests\CreatePaymentRequest;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Project;
use App\Payment;

class PaymentController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $projectId)
    {
        $this->authorize('list payment');
        $project = Project::findOrFail($projectId);
        $payment = $project->payments();

        $trashed = $project->payments()->onlyTrashed();

        $perpage = 10;

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $payment = $trashed;
            }
        }


        if ($request->filled('q')) {
            $q = $request->get('q');
            $payment = $payment->where('payment_no', 'like', '%'.$q.'%');
        }
        
        $payment = $payment->orderBy('created_at', 'DESC')->paginate($perpage);

        return $this->response->paginator($payment, new PaymentTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePaymentRequest $request, $projectId)
    {
        $this->authorize('store payment');
        $project = Project::findOrFail($projectId);

        $payment = $project->payments()->create(array_merge($request->all(), ['payment_no' => getPaymentNo($project)]));

        return response()->json([
            'message' => 'Pembayaran berhsil ditambahkan',
            'status' => 'ok',
            'payment' => $payment
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show payment');
        return $this->response->item(Payment::findOrFail($id), new PaymentTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePaymentRequest $request, $id)
    {
        $this->authorize('update payment');
        $payment = Payment::findOrFail($id);

        $payment->update($request->all());

        return response()->json([
            'message' => 'Pembayaran berhasil diupdate',
            'status' => 'ok',
            'payment' => $payment
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete payment');
        $payment = Payment::findOrFail($id);

        $payment->delete();

        return response()->json([
            'message' => 'Pembayaran berhasil hapus',
            'status' => 'ok',
            'payment' => $payment
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore payment');
        $payment = Payment::onlyTrashed()->where('id', $id)->first();

        $payment->restore();

        return response()->json([
            'message' => 'Payment berhasil direstore!',
            'status' => 'ok',
            'payment' => $payment
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete payment');
        $payment = Payment::onlyTrashed()->where('id', $id)->first();

        $payment->forceDelete();

        return response()->json([
            'message' => 'Payment berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete payment');
        $trashed = Payment::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
