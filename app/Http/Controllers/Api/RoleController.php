<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Role;
use App\Transformers\RoleTransformer;
use App\Http\Requests\CreateRoleRequest;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;

class RoleController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list role');
        $roles = new Role();

        $perpage = 10;
        $trashed = $roles->onlyTrashed();

        $roles = $roles->where('id', '!=', auth()->user()->role->id);

        if ($request->filled('all')) {
            if ($request->get('all') == 1) {
                return $this->response->collection($roles->orderBy('name')->get(), new RoleTransformer);
            }
        }

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $roles = $trashed;
            }
        }

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $roles = $roles->where('name', 'like', '%'.$q.'%');
        }
        
        $roles = $roles->paginate($perpage);

        return $this->response->paginator($roles, new RoleTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoleRequest $request)
    {
        $this->authorize('store role');
        $role = Role::create($request->except('permission_ids'));

        if ($request->filled('permission_ids')) {
            $role->permissions()->attach($request->get('permission_ids'));
        }

        return response()->json([
            'message' => 'Role berhasil dibuat!',
            'staus' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show role');
        $role = Role::findOrFail($id);
        return $this->response->item($role, new RoleTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(CreateRoleRequest $request, $id)
    {
        $this->authorize('update role');
        $role = Role::findOrFail($id);
        $role->update($request->except('permission_ids'));

        if ($request->filled('permission_ids')) {
            $role->permissions()->sync($request->get('permission_ids'));
        }

        return response()->json([
            'message' => 'Role berhasil diupdate!',
            'staus' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete role');
        $role = Role::findOrFail($id);

        if ($role->permissions->count() > 0) {
            $role->permissions()->detach();
        }

        $role->delete();

        return response()->json([
            'message' => 'Role berhasil dihapus!',
            'staus' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore role');
        $role = Role::onlyTrashed()->where('id', $id)->first();

        $role->restore();

        return response()->json([
            'message' => 'Role berhasil direstore!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete role');
        $role = Role::onlyTrashed()->where('id', $id)->first();

        $role->forceDelete();

        return response()->json([
            'message' => 'Role berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete role');
        $trashed = Role::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
