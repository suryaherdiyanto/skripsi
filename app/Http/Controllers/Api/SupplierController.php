<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Supplier;
use App\Transformers\SupplierTransformer;
use App\Http\Requests\CreateSupplierRequest;
use App\Http\Requests\UpdateSupplierRequest;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list supplier');

        $suppliers = new Supplier();
        
        if ($request->filled('all')) {
            if ($request->get('all') == 1) {
                return $this->response->collection($suppliers->orderBy('name')->get(), new SupplierTransformer);
            }
        }

        $perpage = 10;
        $trashed = $suppliers->onlyTrashed();

        
        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $suppliers = $trashed;
            }
        }

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }


        if ($request->filled('q')) {
            $q = $request->get('q');
            $suppliers = $suppliers->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%');
        }
        
        $suppliers = $suppliers->paginate($perpage);

        return $this->response->paginator($suppliers, new SupplierTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSupplierRequest $request)
    {
        $this->authorize('store supplier');

        Supplier::create($request->all());

        return response()->json([
            'message' => 'Supplier berhasil ditambahkan!',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show supplier');

        $supplier = Supplier::findOrFail($id);
        return $this->response->item($supplier, new SupplierTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSupplierRequest $request, $id)
    {
        $this->authorize('update supplier');
        $supplier = Supplier::findOrFail($id);

        $supplier->update($request->all());

        return response()->json([
            'message' => 'Supplier berhasil diupdate!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete supplier');
        Supplier::findOrFail($id)->delete();

        return response()->json([
            'message' => 'Supplier berhasil dihapus!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore supplier');
        $supplier = Supplier::onlyTrashed()->where('id', $id)->first();

        $supplier->restore();

        return response()->json([
            'message' => 'Supplier berhasil direstore!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete supplier');
        $supplier = Supplier::onlyTrashed()->where('id', $id)->first();

        $supplier->forceDelete();

        return response()->json([
            'message' => 'Supplier berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete supplier');
        $trashed = Supplier::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
