<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Permission;
use App\Transformers\PermissionTransformer;
use App\Http\Requests\CreatePermissionRequest;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;

class PermissionController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permissions = new Permission();
        
        if ($request->filled('all')) {
            if ($request->get('all') == 1) {
                return $this->response->collection($permissions->get(), new PermissionTransformer);
            }
        }

        $perpage = 10;

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }


        if ($request->filled('q')) {
            $q = $request->get('q');
            $permissions = $permissions->where('name', 'like', '%'.$q.'%');
        }
        
        $permissions = $permissions->paginate($perpage);

        return $this->response->paginator($permissions, new PermissionTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePermissionRequest $request)
    {
        Permission::create($request->all());

        return response()->json([
            'message' => 'Permission berhasil dibuat!',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::findOrFail($id);
        return $this->response->item($permission, new PermissionTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePermissionRequest $request, $id)
    {
        $permission = Permission::findOrFail($id);
        $permission->update($request->all());

        return response()->json([
            'message' => 'Permission berhasil diedit!',
            'status' => 'ok'
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $permission = Permission::findOrFail($id);
        $permission->delete();
        
        return response()->json([
            'message' => 'Permission berhasil dihapus!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $permission = Permission::onlyTrashed()->where('id', $id)->first();

        $permission->restore();

        return response()->json([
            'message' => 'Permission berhasil direstore permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $permission = Permission::onlyTrashed()->where('id', $id)->first();

        $permission->forceDelete();

        return response()->json([
            'message' => 'Permission berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $trashed = Permission::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
