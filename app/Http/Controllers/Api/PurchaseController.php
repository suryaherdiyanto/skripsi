<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Purchase;
use App\Project;
use App\Transformers\PurchaseTransformer;
use App\Http\Requests\CreatePurchaseRequest;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use PDF;

class PurchaseController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $projectId)
    {
        $this->authorize('list purchase');
        $project = Project::findOrFail($projectId);
        $purchase = $project->purchases();
        $perpage = 10;

        $trashed = $project->purchases()->onlyTrashed();

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $purchase = $trashed;
            }
        }

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $purchase = $purchase->where('purchase_no', 'like', '%'.$q.'%');
        }
        
        $purchase = $purchase->paginate($perpage);

        return $this->response->paginator($purchase, new PurchaseTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePurchaseRequest $request, $projectId)
    {
        $this->authorize('store purchase');
        $project = Project::findOrFail($projectId);

        $purchase = $project->purchases()->create(array_merge($request->except('details'), ['purchase_no' => getPurchaseNo($project)]));

        if ($request->filled('details')) {
            $details = [];
            foreach ($request->get('details') as $key => $detail) {
                if ($detail['item_id'] > 0) {
                    $details[] = [
                        'item_id' => $detail['item_id'],
                        'purchase_price' => $detail['purchase_price'],
                        'qty' => $detail['qty'],
                        'qty_type' => $detail['qty_type'],
                        'total' => $detail['total']
                    ];
                }
            }

            if (count($details) > 0) {
                $purchase->details()->createMany($details);
            }
        }

        return response()->json([
            'message' => 'Pembelian berhsil ditambahkan',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $this->authorize('show purchase');
        $purchase = new Purchase();

        if ($request->filled('pdf')) {
            if ($request->get('pdf') == true) {
                $purchase = $purchase->with(['details', 'payments'])->find($id);

                $filename = 'purchase-report_'.$purchase->purchase_no.'_'.now()->format('Ymd-His').'.pdf';
                $pdf = PDF::loadView('reports.purchase-detail', ['purchase' => $purchase]);
                $pdf->save(public_path() . '/reports/'.$filename);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Laporan berhasil dibuat!',
                    'url' => '/reports/'.$filename
                ], 200);
            }
        }

        return $this->response->item($purchase->find($id), new PurchaseTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CreatePurchaseRequest $request, $id)
    {
        $this->authorize('udpate purchase');
        $purchase = Purchase::findOrFail($id);

        $purchase->update($request->except('details'));

        if ($request->filled('details')) {
            $i = 0;
            $details = $request->get('details');

            foreach ($purchase->details as $detail) {
                $detail->update($details[$i]);
                $i++;
            }
        }

        return response()->json([
            'message' => 'Pembelian berhasil diupdate',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete purchase');
        $purchase = Purchase::findOrFail($id);
        
        $project = $purchase->project;

        $purchase->delete();

        return response()->json([
            'message' => 'Pembelian berhasil hapus',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore purchase');
        $purchase = Purchase::onlyTrashed()->where('id', $id)->first();

        $purchase->restore();

        return response()->json([
            'message' => 'Pembelian berhasil direstore permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete purchase');
        $purchase = Purchase::onlyTrashed()->where('id', $id)->first();

        $purchase->forceDelete();

        return response()->json([
            'message' => 'Pembelian berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete purchase');
        $trashes = Purchase::onlyTrashed()->get();

        foreach ($trashes as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
