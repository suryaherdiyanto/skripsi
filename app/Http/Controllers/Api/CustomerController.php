<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Customer;
use App\Transformers\CustomerTransformer;
use App\Http\Requests\CreateCustomerRequest;
use App\Http\Requests\UpdateCustomerRequest;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list customer');
        $customers = new Customer();
        $trashed = $customers->onlyTrashed();

        if ($request->filled('all')) {
            if ($request->get('all') == 1) {
                return $this->response->collection($customers->orderBy('name')->get(), new CustomerTransformer);
            }
        }

        $perpage = 10;
        
        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $customers = $trashed;
            }
        }
        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $customers = $customers->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%');
        }
        
        $customers = $customers->orderBy('created_at', 'DESC')->paginate($perpage);

        return $this->response->paginator($customers, new CustomerTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateCustomerRequest $request)
    {
        $this->authorize('store customer');
        Customer::create($request->all());

        return response()->json([
            'message' => 'Customer berhasil ditambahkan',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show customer');
        return $this->response->item(Customer::findOrFail($id), new CustomerTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerRequest $request, $id)
    {
        $this->authorize('update customer');
        $customer = Customer::findOrFail($id);

        $customer->update($request->all());

        return response()->json([
            'message' => 'Customer berhasil diupdate',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete customer');
        $customer = Customer::findOrFail($id);

        $customer->delete();

        return response()->json([
            'message' => 'Customer berhasil dihapus',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore customer');
        $customer = Customer::onlyTrashed()->where('id', $id)->first();

        $customer->restore();

        return response()->json([
            'message' => 'Customer berhasil direstore!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete customer');
        $customer = Customer::onlyTrashed()->where('id', $id)->first();

        $customer->forceDelete();

        return response()->json([
            'message' => 'Customer berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete customer');
        $trashed = Customer::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
