<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Item;
use App\Purchase;
use App\Project;
use App\Transformers\ItemTransformer;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list item');
        $items = new Item();
        $trashed = $items->onlyTrashed();

        if ($request->filled('all')) {

            if ($request->filled('in_purchase_entry')) {
                if ($request->get('in_purchase_entry') == 'false') {

                    if ($request->filled('purchase_id')) {
                        $items = $items->whereDoesntHave('purchaseDetails', function($q) use($request) {
                            $q->where('purchase_id', '=', $request->get('purchase_id'));
                        });
                    } else {
                        $purchases = Project::findOrFail($request->get('project_id'))->purchases;

                        $items = $items->whereDoesntHave('purchaseDetails', function($q) use($purchases) {
                            $q->whereIn('purchase_id', $purchases->map(function($item) {
                                return $item->id;
                            }));
                        });
                    }

                }
            }

            $items = $items->orderBy('name');

            if ($request->get('all') == 'true') {
                $items = $items->get();
            } else {
                $items = $items->paginate(10);
            }

            return $this->response->collection($items, new ItemTransformer);
        }
        
        $perpage = 10;
        
        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $items = $trashed;
            }
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $items = $items->where('name', 'like', '%'.$q.'%')->orWhere('code', 'like', '%'.$q.'%');
        }
        
        $items = $items->paginate($perpage);

        return $this->response->paginator($items, new ItemTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateItemRequest $request)
    {
        $this->authorize('store item');
        Item::create($request->all());

        return response()->json([
            'message' => 'Barang berhasil ditambahkan!',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show item');
        return $this->response->item(Item::findOrFail($id), new ItemTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateItemRequest $request, $id)
    {
        $this->authorize('update item');
        $item = Item::findOrFail($id);

        $item->update($request->all());

        return response()->json([
            'message' => 'Barang berhasil diupdate!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete item');
        $item = Item::findOrFail($id);

        $item->delete();

        return response()->json([
            'message' => 'Barang berhasil dihapus!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore item');
        $item = Item::onlyTrashed()->where('id', $id)->first();

        $item->restore();

        return response()->json([
            'message' => 'Item berhasil direstore permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete item');
        $item = Item::onlyTrashed()->where('id', $id)->first();

        $item->forceDelete();

        return response()->json([
            'message' => 'Item berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete item');
        $trashed = Item::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
