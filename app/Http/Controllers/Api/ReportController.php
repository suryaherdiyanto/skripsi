<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use PDF;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function projectReport(Request $request)
    {
        $this->authorize('view report');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $data = DB::table('projects')->selectRaw('projects.id, 
                                                    projects.name, 
                                                    customers.name as customer_name, 
                                                    projects.project_no,
                                                    projects.start_date,

                                                    CONCAT("Rp. ", FORMAT(projects.total_cost, 0)) as total_cost, 
                                                    CONCAT("Rp. ", FORMAT(projects.total_paid, 0)) as total_paid, 
                                                    CONCAT("Rp. ", FORMAT(projects.total_payment, 0)) as total_payment,
                                                    CONCAT("Rp. ", FORMAT((projects.total_paid - projects.total_cost), 0)) as profit');
        
        $dataSubtotal = DB::table('projects')->selectRaw('
                                                    CONCAT("Rp. ", FORMAT(SUM(projects.total_cost), 0)) as cost_subtotal, 
                                                    CONCAT("Rp. ", FORMAT(SUM(projects.total_paid), 0)) as paid_subtotal, 
                                                    CONCAT("Rp. ", FORMAT(SUM(projects.total_payment), 0)) as payment_subtotal,
                                                    CONCAT("Rp. ", FORMAT(SUM(projects.total_paid - projects.total_cost), 0)) as profit_subtotal');

        $data = $data->whereRaw('projects.deleted_at IS NULL AND projects.payment_status >= 1 AND (projects.total_paid - projects.total_cost) > 0');
        $dataSubtotal = $dataSubtotal->whereRaw('projects.deleted_at IS NULL AND projects.payment_status >= 1 AND (projects.total_paid - projects.total_cost) > 0');
        
        if ($startDate) {
            $data = $data->whereRaw('start_date >= "'.$startDate.'"');
            $dataSubtotal = $dataSubtotal->whereRaw('start_date >= "'.$startDate.'"');
        }

        if ($endDate) {
            $data = $data->whereRaw('start_date <= "'.$endDate.'"');
            $dataSubtotal = $dataSubtotal->whereRaw('start_date <= "'.$endDate.'"');
        }

        $dataSubtotal = $dataSubtotal->leftJoin('customers', 'projects.customer_id', '=', 'customers.id');
        $data = $data->leftJoin('customers', 'projects.customer_id', '=', 'customers.id');

        $dataSubtotal = $dataSubtotal->whereRaw('total_cost > 0')->orderBy('start_date', 'ASC')->first();
        $data = $data->whereRaw('total_cost > 0')->orderBy('start_date', 'ASC')->get();

        if ($request->filled('pdf')) {
            if ($request->get('pdf') == true) {

                $start_date = Carbon::createFromFormat('Y/m/d', $startDate)->locale('id');
                $end_date = Carbon::createFromFormat('Y/m/d', $endDate)->locale('id');

                $pdf = PDF::loadView('reports.project', ['projects' => $data, 'start_date' => $start_date, 'end_date' => $end_date, 'subtotal' => $dataSubtotal]);
                $filename = 'project-report_'.now()->format('Ymd-His').'.pdf';

                $pdf->save(public_path() . '/reports/'.$filename);

                return response()->json([
                    'status' => 'success',
                    'message' => 'File PDF berhasil dibuat',
                    'url' => asset('/reports/'.$filename)
                ], 200);
            }
        }

        return response()->json([
            'message' => 'success',
            'data' => $data,
            'subtotal' => $dataSubtotal
        ], 200);
    }

    public function profitReport(Request $request)
    {
        $this->authorize('view report');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $data = DB::table('projects')->selectRaw('MONTH(start_date) as month, CONCAT("Rp. ", FORMAT(SUM(total_paid - total_cost), 0)) profit');

        $data = $data->whereRaw('payment_status = 1 AND (total_paid - total_cost) > 0');

        if ($startDate) {
            $data = $data->whereRaw('start_date >= "'.$startDate.'"');
        }

        if ($endDate) {
            $data = $data->whereRaw('start_date <= "'.$endDate.'"');
        }

        $data = $data->orderBy('month', 'ASC')->groupBy(DB::raw('MONTH(start_date)'))->get();

        return response()->json([
            'data' => $data,
            'message' => 'success'
        ], 200);

    }

    public function paymentReport(Request $request)
    {
        $this->authorize('view report');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $data = DB::table('payments')->selectRaw('payments.id,
                                                    payments.payment_no,
                                                    DATE_FORMAT(payments.date, "%d %M %Y") as date,
                                                    CONCAT("Rp. ", FORMAT(total, 0)) as total,
                                                    projects.project_no,
                                                    projects.id'
                                                );

        if ($startDate) {
            $data = $data->whereRaw('date >= "'.$startDate.'"');
        }

        if ($endDate) {
            $data = $data->whereRaw('date <= "'.$endDate.'"');
        }

        $data = $data->leftJoin('projects', 'payments.project_id', '=', 'projects.id')->orderBy('payments.date', 'ASC')->get();

        if ($request->filled('pdf')) {
            if ($request->get('pdf') == true) {
                $start_date = Carbon::createFromFormat('Y-m-d', $startDate)->locale('id');
                $end_date = Carbon::createFromFormat('Y-m-d', $endDate)->locale('id');

                $pdf = PDF::loadView('reports.payment', ['payments' => $data, 'start_date' => $start_date, 'end_date' => $end_date]);
                $filename = 'payment-report_'.now()->format('Ymd-His').'.pdf';

                $pdf->save(public_path() . '/reports/'.$filename);

                return response()->json([
                    'status' => 'success',
                    'message' => 'File PDF berhasil dibuat',
                    'url' => asset('/reports/'.$filename)
                ], 200);
            }
        }

        return response()->json([
            'message' => 'success',
            'data' => $data
        ]);
    }

    public function purchaseReport(Request $request)
    {
        $this->authorize('view report');
        $startDate = $request->get('start_date');
        $endDate = $request->get('end_date');

        $data = DB::table('purchases')->selectRaw('purchases.id as id,
                                                    purchases.purchase_no as purchase_no,
                                                    purchases.date as date,
                                                    CONCAT("Rp. ", FORMAT(purchases.total_paid, 0)) as total_paid,
                                                    CONCAT("Rp. ", FORMAT(purchases.total, 0)) as total,
                                                    CONCAT("Rp. ", FORMAT((purchases.total - purchases.total_paid), 0)) as remain_balance,
                                                    projects.project_no as project_no');

        $dataSubtotal = DB::table('purchases')->selectRaw('CONCAT("Rp. ", FORMAT(SUM(purchases.total_paid), 0)) as paid_subtotal,
                                                    CONCAT("Rp. ", FORMAT(SUM(purchases.total), 0)) as subtotal,
                                                    CONCAT("Rp. ", FORMAT(SUM(purchases.total - purchases.total_paid), 0)) as remain_balance_subtotal
                                                    ');
        
        $data = $data->whereRaw('purchases.deleted_at IS NULL');
        $dataSubtotal = $dataSubtotal->whereRaw('purchases.deleted_at IS NULL');

        if ($startDate) {
            $data = $data->whereRaw('date >= "'.$startDate.'"');
            $dataSubtotal = $dataSubtotal->whereRaw('date >= "'.$startDate.'"');
        }

        if ($endDate) {
            $data = $data->whereRaw('date <= "'.$endDate.'"');
            $dataSubtotal = $dataSubtotal->whereRaw('date <= "'.$endDate.'"');
        }

        $data = $data->leftJoin('projects', 'purchases.project_id', '=', 'projects.id')->orderBy('purchases.date', 'DESC')->get();
        $dataSubtotal = $dataSubtotal->leftJoin('projects', 'purchases.project_id', '=', 'projects.id')->orderBy('purchases.date', 'DESC')->first();

        if ($request->filled('pdf')) {
            if ($request->get('pdf') == true) {
                $start_date = Carbon::createFromFormat('Y-m-d', $startDate)->locale('id');
                $end_date = Carbon::createFromFormat('Y-m-d', $endDate)->locale('id');

                $pdf = PDF::loadView('reports.purchase', ['purchases' => $data, 'start_date' => $start_date, 'end_date' => $end_date, 'datasubtotal' => $dataSubtotal]);
                $filename = 'purchase-report_'.now()->format('Ymd-His').'.pdf';

                $pdf->save(public_path() . '/reports/'.$filename);

                return response()->json([
                    'status' => 'success',
                    'message' => 'File PDF berhasil dibuat',
                    'url' => asset('/reports/'.$filename)
                ], 200);
            }
        }

        return response()->json([
            'message' => 'success',
            'data' => $data,
            'datasubtotal' => $dataSubtotal
        ]);
    }

    public function incomeOutcomeReport(Request $request)
    {
        $this->authorize('view report');
        
        $income = DB::table('payments')->selectRaw('MONTH(date) as month, YEAR(date) as year, sum(total) as total');
        $outcome = DB::table('purchase_payments')->selectRaw('MONTH(date) as month, YEAR(date) as year, sum(total) as total');
        
        $incomeYears = $income->groupBy(DB::raw('YEAR(date)'))->pluck('year');
        $outcomeYears = $outcome->groupBy(DB::raw('YEAR(date)'))->pluck('year');
        $years = array_unique(array_merge($incomeYears->toArray(), $outcomeYears->toArray()));

        if ($request->filled('year')) {
            $year = $request->get('year');
            $income = $income->whereRaw('year(date) = '.$year);
            $outcome = $outcome->whereRaw('year(date) = '.$year);
        }

        $income = $income->orderBy('month', 'ASC')->groupBy(DB::raw('MONTH(date)'))->get();
        $outcome = $outcome->orderBy('month', 'ASC')->groupBy(DB::raw('MONTH(date)'))->get();
        

        return response()->json([
            'message' => 'success',
            'report' => [
                'income'    => $income,
                'outcome'   => $outcome,
                'years'     => $years
            ]
        ], 200);
    }

    public function outcomeReport(Request $request)
    {
        $this->authorize('view report');
        $year = $request->get('year');

        if ($year) {
            $data = $data->whereRaw('year(date) = '.$year);
        }

        $data = $data->orderBy('month', 'ASC')->groupBy(DB::raw('MONTH(date)'))->get();

        return response()->json([
            'message' => 'success',
            'data' => $data
        ], 200);
    }

    public function total()
    {
        $totalItems = \App\Item::count();
        $totalProject = \App\Project::count();
        $totalCustomer = \App\Customer::count();
        $totalSupplier = \App\Supplier::count();

        return response()->json([
            'data' => [
                'total_items' => $totalItems,
                'total_project' => $totalProject,
                'total_customer' => $totalCustomer,
                'total_supplier' => $totalSupplier
            ],
            'status' => 'success'
        ], 200);
    }
}
