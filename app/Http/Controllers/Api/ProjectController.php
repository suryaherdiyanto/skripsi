<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Project;
use App\Purchase;
use App\Payment;
use App\PurchasePayment;
use App\Transformers\ProjectTransformer;
use App\Http\Requests\CreateProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Events\ProjectWasCreated;
use App\Events\BeforeProjectDeleted;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use PDF;

class ProjectController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list project');
        $user = auth()->user();

        $project = new Project();
        $perpage = 10;
        $trashed = $project->onlyTrashed();

        if ($user->role->name === 'staf') {
            $project->where('user_id', auth()->user()->id);
        }

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $project = $trashed;
            }
        }
        
        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $project = $project->where('name', 'like', '%'.$q.'%')->orWhere('project_no', 'like', '%'.$q.'%');
        }
        
        $project = $project->orderBy('created_at', 'DESC')->paginate($perpage);

        $now = now();

        $totalPurchase = Purchase::whereMonth('date', $now->month)->whereYear('date', $now->year)->sum('total');
        $totalPurchasePayment = PurchasePayment::whereMonth('date', $now->month)->whereYear('date', $now->year)->sum('total');
        $totalPayment = Payment::whereMonth('date', $now->month)->whereYear('date', $now->year)->sum('total');

        return $this->response->paginator($project, new ProjectTransformer)
                                ->addMeta('total_purchase', 'Rp. ' . number_format($totalPurchase, 0))
                                ->addMeta('total_paid_purchase', 'Rp. ' . number_format($totalPurchasePayment, 0))
                                ->addMeta('total_payment', 'Rp. ' . number_format($totalPayment, 0))
                                ->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateProjectRequest $request)
    {
        $this->authorize('store project');
        $project = Project::create(array_merge($request->all(), ['user_id' => auth()->user()->id]));

        return response()->json([
            'message' => 'Proyek berhasil ditambahkan!',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        $this->authorize('show project');
        $project = Project::findOrFail($id);

        $totalPurchases = $project->purchases()->where('status', '>', 0)->sum('total_paid');
        $remainPurchases = $project->purchases()->selectRaw('sum(purchases.total - purchases.total_paid) as total_remain')->whereRaw('status IN(0,1)')->first();

        if ($request->filled('pdf')) {
            if ($request->get('pdf') == true) {

                $filename = 'project-report_'.$project->project_no.'_'.now()->format('Ymd-His').'.pdf';
                $pdf = PDF::loadView('reports.project-detail', ['project' => $project, 'total_purchases' => number_format($totalPurchases), 'remain_purchase' => number_format($remainPurchases->total_remain)]);
                $pdf->save(public_path() . '/reports/'.$filename);

                return response()->json([
                    'status' => 'success',
                    'message' => 'Laporan berhasil dibuat!',
                    'url' => '/reports/'.$filename
                ], 200);
            }
        }

        return $this->response->item($project, new ProjectTransformer)
                        ->addMeta('total_purchases_complete', 'Rp. '.number_format($totalPurchases))
                        ->addMeta('remain_purchases', 'Rp. '.number_format($remainPurchases->total_remain));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProjectRequest $request, $id)
    {
        $this->authorize('update project');
        $project = Project::findOrFail($id);

        $project->update($request->only('name', 'start_date', 'description', 'customer_id'));

        return response()->json([
            'message' => 'Proyek berhasil diupdate!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete project');
        $project = Project::findOrFail($id);

        $project->delete();

        return response()->json([
            'message' => 'Proyek berhasil dihapus!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore project');
        $project = Project::onlyTrashed()->where('id', $id)->first();

        $project->restore();

        return response()->json([
            'message' => 'Proyek berhasil direstore permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete project');
        $project = Project::onlyTrashed()->where('id', $id)->first();

        $project->clearPurchases(true);
        $project->clearPayments(true);

        $project->forceDelete();

        return response()->json([
            'message' => 'Project berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete project');
        $trashed = Project::onlyTrashed()->get();

        foreach ($trashed as $data) {
            $data->clearPurchases(true);
            $data->clearPayments(true);

            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
