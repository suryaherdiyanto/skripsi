<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\PurchasePaymentTransformer;
use App\Http\Requests\CreatePurchasePaymentRequest;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Purchase;
use App\PurchasePayment;

class PurchasePaymentController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $purchaseId)
    {
        $this->authorize('list payment');
        $purchase = Purchase::findOrFail($purchaseId);

        $payment = $purchase->payments();
        $perpage = 10;

        $trashed = $purchase->payments()->onlyTrashed();

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $payment = $trashed;
            }
        }

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $payment = $payment->where('payment_no', 'like', '%'.$q.'%');
        }
        
        $payment = $payment->orderBy('date', 'DESC')->paginate($perpage);

        return $this->response->paginator($payment, new PurchasePaymentTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePurchasePaymentRequest $request, $purchaseId)
    {
        $this->authorize('store payment');
        $purchase = Purchase::findOrFail($purchaseId);

        $data['payment_no'] = getPaymentNo($purchase);

        if (!$request->filled('date')) {
            $data['date'] = now()->format('Y-m-d');
        }

        $purchase->payments()->create(array_merge($request->all(), $data));

        return response()->json([
            'message' => 'Pembayaran berhasil ditambahkan',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show payment');
        return $this->response->item(PurchasePayment::findOrFail($id), new PurchasePaymentTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update payment');
        $payment = PurchasePayment::findOrFail($id);

        $payment->update($request->all());

        return response()->json([
            'message' => 'Pembayaran berhasil diupdate',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete payment');
        $payment = PurchasePayment::findOrFail($id);

        $payment->delete();

        return response()->json([
            'message' => 'Pembayaran berhasil hapus',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore payment');
        $payment = PurchasePayment::onlyTrashed()->where('id', $id)->first();

        $payment->restore();

        return response()->json([
            'message' => 'Payment berhasil direstore permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete payment');
        $payment = PurchasePayment::onlyTrashed()->where('id', $id)->first();

        $payment->forceDelete();

        return response()->json([
            'message' => 'Payment berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete payment');
        $trashes = PurchasePayment::onlyTrashed()->get();

        foreach ($trashes as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
