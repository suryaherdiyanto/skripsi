<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\PurchaseDetail;
use App\Purchase;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\PurchaseDetailTransformer;

class PurchaseDetailController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($purchaseId, Request $request)
    {
        $this->authorize('list purchase');
        $purchase = Purchase::findOrFail($purchaseId);
        $detail = $purchase->details();
        $perpage = 10;

        $trashed = PurchaseDetail::onlyTrashed();

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $detail = $trashed;
            }
        }

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $detail = $detail->item()->where('code', $q);
        }
        
        $detail = $detail->paginate($perpage);

        return $this->response->paginator($detail, new PurchaseDetailTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $purchaseId)
    {
        $data = $request->get('details');
        $purchase = \App\Purchase::findOrFail($purchaseId);

        if (count($data) === 1) {
            $purchase->details()->create($data[0]);
        } else {
            $purchase->details()->createMany($data);
        }

        return response()->json([
            'status' => 'ok',
            'message' => 'Detail pembelian berhasil ditambahkan!'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->response->item(PurchaseDetail::findOrFail($id), new PurchaseDetailTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $purchaseDetail = PurchaseDetail::findOrFail($id);

        $purchaseDetail->update($request->all());

        return response()->json([
            'status' => 'ok',
            'message' => 'Detail pembelian berhasil diupdate!'
        ], 200); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $purchaseDetail = PurchaseDetail::findOrFail($id);

        $purchaseDetail->delete();

        return response()->json([
            'status' => 'ok',
            'message' => 'Detail pembelian berhasil dihapus!'
        ], 200); 
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore purchase');
        $purchaseDetail = PurchaseDetail::onlyTrashed()->where('id', $id)->first();

        $purchaseDetail->restore();

        return response()->json([
            'status' => 'ok',
            'message' => 'Detail pembelian berhasil direstore!'
        ], 200); 
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete($id)
    {
        $this->authorize('force-delete purchase');
        $purchaseDetail = PurchaseDetail::onlyTrashed()->where('id', $id)->first();

        $purchaseDetail->forceDelete();

        return response()->json([
            'status' => 'ok',
            'message' => 'Detail pembelian berhasil dihapus permanen!'
        ], 200); 
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete purchase');
        $trashes = PurchaseDetail::onlyTrashed()->get();

        foreach ($trashes as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
