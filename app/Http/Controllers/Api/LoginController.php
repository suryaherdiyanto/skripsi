<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use \App\Http\Controllers\Controller;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserTransformer;
use App\Transformers\PermissionTransformer;
use JWTAuth;

class LoginController extends Controller
{
    use AuthenticatesUsers, Helpers;

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(Request $request)
    {

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
            
        $credentials = $request->only(['email', 'password']);
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['message' => 'Login gagal, periksa kembali username dan password anda', 'status' => 'unauhtenticated'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        $user = auth()->user();
        return $this->response->item($user, new UserTransformer())->addMeta('access_token', auth()->tokenById($user->id));
    }

    protected function respondWithToken($token)
    {
        $user = auth()->user();

        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => array_merge($user->toArray(), ['permissions' => $user->getPermissions()])
        ]);
    }

    public function getPermissions()
    {
        return $this->response->collection(auth()->user()->role->permissions, new PermissionTransformer);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function passwordReset(Request $request)
    {
        $request->validate([
            'password' => 'required|string',
            'new_password' => 'required|string|confirmed'
        ]);

        $token = auth()->attempt($request->only(['email', 'password']));

        if (!$token) {
            return response()->json([
                'message' => 'User '.$request->get('email').', password salah!',
                'status' => 'not found'
            ], 404);
        }

        $user = JWTAuth::toUser($token);

        $user->password = $request->get('new_password');
        $user->save();

        return response()->json([
            'message' => 'Password telah berhasil diganti!',
            'status' => 'success'
        ], 200);
    }
}
