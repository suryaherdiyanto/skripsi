<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use App\Transformers\UserTransformer;
use App\Transformers\PermissionTransformer;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;


class UserController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('list user');

        $users = new User();
        $perpage = 10;
        $trashed = $users->onlyTrashed();

        $users = $users->where('id', '!=', auth()->user()->id);

        if ($request->filled('perpage')) {
            $perpage = $request->get('perpage');
        }

        if ($request->filled('trashed')) {
            if ($request->get('trashed') == 1) {
                $users = $trashed;
            }
        }

        if ($request->filled('q')) {
            $q = $request->get('q');
            $users = $users->where('name', 'like', '%'.$q.'%')->orWhere('email', 'like', '%'.$q.'%');
        }
        
        $users = $users->paginate($perpage);

        return $this->response->paginator($users, new UserTransformer)->addMeta('trash_count', $trashed->count());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateUserRequest $request)
    {
        $this->authorize('store user');
        User::create($request->all());

        return response()->json([
            'message' => 'User berhasil ditambahkan!',
            'status' => 'ok'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('show user');
        $user = User::findOrFail($id);
        return $this->response->item($user, new UserTransformer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        $this->authorize('update user');
        $user = User::findOrFail($id);
        $user->update($request->all());

        return response()->json([
            'message' => 'User berhasil diupdate!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete user');
        User::findOrFail($id)->delete();

        return response()->json([
            'message' => 'User berhasil dihapus!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Restore user in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $this->authorize('restore user');
        $user = User::onlyTrashed()->where('id', $id)->first();

        $user->restore();

        return response()->json([
            'message' => 'User berhasil direstore!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Force delete in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function forceDelete(Request $request, $id)
    {
        $this->authorize('force-delete user');
        $user = User::onlyTrashed()->where('id', $id)->first();

        $user->forceDelete();

        return response()->json([
            'message' => 'User berhasil dihapus permanen!',
            'status' => 'ok'
        ], 200);
    }

    /**
     * Clear all item in trash.
     *
     * @return \Illuminate\Http\Response
     */
    public function clearTrash()
    {
        $this->authorize('force-delete user');
        $trashes = User::onlyTrashed()->get();

        foreach ($trashes as $data) {
            $data->forceDelete();
        }

        return response()->json([
            'message' => 'Sampah berhasil dibersihkan!',
            'status' => 'ok'
        ], 200);
    }
}
