<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'id',
        'name',
        'email',
        'contact_person',
        'phone',
        'address'
    ];

    public function projects()
    {
        return $this->hasMany(Project::class);
    }
}
