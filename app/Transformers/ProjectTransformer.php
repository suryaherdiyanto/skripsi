<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Project;

class ProjectTransformer extends TransformerAbstract {

    protected $availableIncludes = [
        'payments',
        'purchases',
        'customer'
    ];
    
    public function transform(Project $project)
    {
        return [
            'id' => $project->id,
            'project_no' => $project->project_no,
            'name' => $project->name,
            'start_date' => $project->start_date->format('Y-m-d'),
            'start_date_format' => $project->start_date->isoFormat('LL'),
            'total_cost' => $project->total_cost,
            'total_cost_format' => $project->totalCostFormat(),
            'total_paid' => $project->total_paid,
            'total_paid_format' => $project->totalPaidFormat(),
            'payment_status' => $project->payment_status,
            'payment_status_text' => $project->paymentStatusText(),
            'total_payment' => $project->total_payment,
            'total_payment_format' => 'Rp. '.number_format($project->total_payment),
            'remain_payment' => $project->remainPayment(),
            'remain_payment_format' => 'Rp. '.number_format($project->remainPayment()),
            'profit_loss' => $project->profitAndLoss(),
            'description' => $project->description ?? '-',
            'customer_id' => $project->customer_id,
            'created_at' => $project->created_at->format('d M y H:i:s'),
            'updated_at' => $project->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeCustomer(Project $project)
    {
        if ($project->customer) {
            return $this->item($project->customer, new CustomerTransformer);
        }
    }

    public function includePayments(Project $project)
    {
        if ($project->payments) {
            return $this->collection($project->payments, new PaymentTransformer);
        }
    }

    public function includePurchases(Project $project)
    {
        if ($project->purchases) {
            return $this->collection($project->purchases, new PurchaseTransformer);
        }
    }

}