<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Item;

class ItemTransformer extends TransformerAbstract {

    protected $availableIncludes = ['supplier'];
    
    public function transform(Item $item)
    {
        return [
            'id' => $item->id,
            'code' => $item->code,
            'name' => $item->name,
            'sell_price' => $item->sell_price,
            'purchase_price' => $item->purchase_price,
            'format_sell_price' => $item->formatPrice('sell_price'),
            'format_purchase_price' => $item->formatPrice('purchase_price'),
            'qty_type' => $item->qty_type,
            'description' => $item->description,
            'supplier_id' => $item->supplier_id,
            'created_at' => $item->created_at->format('d M y H:i:s'),
            'updated_at' => $item->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeSupplier(Item $item)
    {
        return $this->item($item->supplier, new SupplierTransformer);
    }

}