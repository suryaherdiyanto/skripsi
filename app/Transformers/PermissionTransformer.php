<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Permission;

class PermissionTransformer extends TransformerAbstract {

    protected $availableIncludes = ['permissions'];
    
    public function transform(Permission $permission)
    {
        return [
            'id' => $permission->id,
            'name' => $permission->name,
            'created_at' => $permission->created_at->format('d M y H:i:s'),
            'updated_at' => $permission->updated_at->format('d M y H:i:s')
        ];
    }

    public function includePermissions(Permission $permission)
    {
        if ($permission->permissions) {
            return $this->collection($permission->permissions, new PermissionController);
        }
    }

}