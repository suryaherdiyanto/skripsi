<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Customer;

class CustomerTransformer extends TransformerAbstract {

    protected $availableIncludes = ['projects'];
    
    public function transform(Customer $customer)
    {
        return [
            'id' => $customer->id,
            'name' => $customer->name,
            'contact_person' => $customer->contact_person,
            'email' => $customer->email,
            'phone' => $customer->phone,
            'address' => $customer->address,
            'created_at' => $customer->created_at->format('d M y H:i:s'),
            'updated_at' => $customer->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeItems(Customer $customer)
    {
        if ($customer->projects) {
            return $this->collection($customer->projects, new ItemTransformer);
        }
    }

}