<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\PurchasePayment;

class PurchasePaymentTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['purchase'];
    
    public function transform(PurchasePayment $payment)
    {
        return [
            'id' => $payment->id,
            'payment_no' => $payment->payment_no,
            'date' => $payment->date->format('Y-m-d'),
            'date_format' => $payment->date->isoFormat('LL'),
            'total' => $payment->total,
            'total_format' => $payment->totalFormat(),
            'created_at' => $payment->created_at->format('d M y H:i:s'),
            'updated_at' => $payment->updated_at->format('d M y H:i:s')
        ];
    }

    public function includePurchase(PurchasePayment $payment)
    {
        return $this->item($payment->purchase, new PurchaseTransformer);
    }

}