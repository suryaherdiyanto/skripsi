<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Purchase;

class PurchaseTransformer extends TransformerAbstract {

    protected $availableIncludes = ['details', 'project', 'payments'];
    
    public function transform(Purchase $purchase)
    {
        return [
            'id' => $purchase->id,
            'purchase_no' => $purchase->purchase_no,
            'date' => $purchase->date->isoFormat('LL'),
            'total' => $purchase->total,
            'total_format' => $purchase->totalFormat(),
            'total_paid' => $purchase->total_paid,
            'total_paid_format' => $purchase->totalPaidFormat(),
            'remain_balance' => $purchase->remainBalance(),
            'remain_balance_format' => 'Rp. ' . number_format($purchase->remainBalance()),
            'status' => $purchase->status,
            'status_text' => $purchase->getStatusText(),
            'created_at' => $purchase->created_at->format('d M y H:i:s'),
            'updated_at' => $purchase->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeProject(Purchase $purchase)
    {
        return $this->item($purchase->project, new ProjectTransformer);
    }

    public function includePayments(Purchase $purchase)
    {
        if ($purchase->payments) {
            return $this->collection($purchase->payments, new PurchasePaymentTransformer);
        }
    }

    public function includeDetails(Purchase $purchase)
    {
        if ($purchase->details) {
            return $this->collection($purchase->details, new PurchaseDetailTransformer);
        }
    }

}