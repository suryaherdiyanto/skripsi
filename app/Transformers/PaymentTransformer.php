<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Payment;

class PaymentTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['project'];
    
    public function transform(Payment $payment)
    {
        return [
            'id' => $payment->id,
            'payment_no' => $payment->payment_no,
            'date' => $payment->date->format('Y-m-d'),
            'date_format' => $payment->date->isoFormat('LL'),
            'total' => $payment->total,
            'total_format' => $payment->totalFormat(),
            'project_id' => $payment->project_id,
            'created_at' => $payment->created_at->format('d M y H:i:s'),
            'updated_at' => $payment->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeProject(Payment $payment)
    {
        return $this->item($payment->project, new ProjectTransformer);
    }

}