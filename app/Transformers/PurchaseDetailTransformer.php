<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\PurchaseDetail;

class PurchaseDetailTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['item'];

    protected $availableIncludes = ['purchase'];
    
    public function transform(PurchaseDetail $detail)
    {
        return [
            'id' => $detail->id,
            'item_id' => $detail->item_id,
            'qty' => $detail->qty,
            'total_purchase_price' => $detail->getTotalPurchasePrice(),
            'total_sell_price' => $detail->getTotalSellPrice(),
            'total_purchase_price_format' => $detail->totalPurchaseFormat(),
            'total_sell_price_format' => $detail->totalSellFormat(),
            'created_at' => $detail->created_at->format('d M y H:i:s'),
            'updated_at' => $detail->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeItem(PurchaseDetail $detail)
    {
        return $this->item($detail->item, new ItemTransformer);
    }

    public function includePurchase(PurchaseDetail $detail)
    {
        return $this->collection($detail->purchase, new PurchaseTransformer);
    }

}