<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\User;

class UserTransformer extends TransformerAbstract {

    protected $defaultIncludes = ['role'];
    
    public function transform(User $user)
    {
        return [
            'id' => $user->id,
            'name' => $user->name,
            'email' => $user->email,
            'created_at' => $user->created_at->format('d M y H:i:s'),
            'updated_at' => $user->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeRole(User $user)
    {
        if ($user->role) {
            return $this->item($user->role, new RoleTransformer);
        }
    }

}