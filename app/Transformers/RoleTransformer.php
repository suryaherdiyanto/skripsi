<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Role;

class RoleTransformer extends TransformerAbstract {

    protected $availableIncludes = ['permissions'];
    
    public function transform(Role $role)
    {
        return [
            'id' => $role->id,
            'name' => $role->name,
            'created_at' => $role->created_at->format('d M y H:i:s'),
            'updated_at' => $role->updated_at->format('d M y H:i:s')
        ];
    }

    public function includePermissions(Role $role)
    {
        if ($role->permissions) {
            return $this->collection($role->permissions, new PermissionTransformer);
        }
    }

}