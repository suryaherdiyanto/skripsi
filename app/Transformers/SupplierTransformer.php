<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Supplier;

class SupplierTransformer extends TransformerAbstract {

    protected $availableIncludes = ['items'];
    
    public function transform(Supplier $supplier)
    {
        return [
            'id' => $supplier->id,
            'name' => $supplier->name,
            'email' => $supplier->email,
            'fax' => $supplier->fax,
            'phone' => $supplier->phone,
            'address' => $supplier->address,
            'created_at' => $supplier->created_at->format('d M y H:i:s'),
            'updated_at' => $supplier->updated_at->format('d M y H:i:s')
        ];
    }

    public function includeItems(Supplier $supplier)
    {
        if ($supplier->items) {
            return $this->collection($supplier->items, new ItemTransformer);
        }
    }

}