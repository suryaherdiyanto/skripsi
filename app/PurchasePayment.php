<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchasePayment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'payment_no',
        'date',
        'total',
        'purchase_id'
    ];

    protected $dates = [
        'date'
    ];

    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    public function totalFormat()
    {
        return 'Rp. ' . number_format($this->attributes['total'], 0);
    }
}
