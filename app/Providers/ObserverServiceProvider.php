<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Purchase;
use App\Payment;
use App\PurchasePayment;
use App\PurchaseDetail;
use App\Project;
use App\Observers\PurchaseObserver;
use App\Observers\PaymentObserver;
use App\Observers\PurchaseDetailsObserver;
use App\Observers\PurchasePaymentObserver;
use App\Observers\ProjectObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Purchase::observe(PurchaseObserver::class);
        Payment::observe(PaymentObserver::class);
        PurchasePayment::observe(PurchasePaymentObserver::class);
        PurchaseDetail::observe(PurchaseDetailsObserver::class);
        Project::observe(ProjectObserver::class);
    }
}
