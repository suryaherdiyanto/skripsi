<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'purchase_no',
        'date',
        'total',
        'total_paid',
        'status',
        'project_id'
    ];

    private $statusText = [
        '0' => 'Belum Dibayar',
        '1' => 'Dibayar Sebagian',
        '2' => 'Lunas'
    ];

    protected $dates = [
        'date'
    ];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function details()
    {
        return $this->hasMany(PurchaseDetail::class);
    }

    public function payments()
    {
        return $this->hasMany(PurchasePayment::class);
    }

    public function totalPayment()
    {
        return $this->payments()->sum('total');
    }

    public function remainBalance()
    {
        return $this->total - $this->total_paid;
    }

    public function totalFormat()
    {
        return 'Rp. ' . number_format($this->attributes['total'], 0);
    }

    public function totalPaidFormat()
    {
        return 'Rp. ' . number_format($this->attributes['total_paid'], 0);
    }

    public function remainBalanceFormat()
    {
        return 'Rp. ' . number_format($this->remainBalance(), 0);
    }

    public function getStatusText()
    {
        return $this->statusText[$this->status];
    }

    public function dateFormat()
    {
        return Carbon::createFromFormat('Y-m-d', $this->attributes['date'])->format('d F Y');
    }

    public function clearPayments($forceDelete = false)
    {
        if ($forceDelete) {
            foreach ($this->payments()->onlyTrashed()->get() as $payment) {
                $payment->forceDelete();
            }
        } else {
            if ($this->payments) {
                foreach ($this->payments as $payment) {
                    $payment->delete();
                }
            }
        }
    }

    public function clearDetails($forceDelete = false)
    {
        if ($forceDelete) {
            foreach ($this->details()->onlyTrashed()->get() as $detail) {
                $detail->forceDelete();
            }
        } else {
            if ($this->details) {
                foreach ($this->details as $detail) {
                    $detail->delete();
                }
            }
        }
    }

    public function restoreDetails()
    {
        $details = $this->details()->onlyTrashed()->get();

        if ($details->count() > 0) {
            foreach ($details as $detail) {
                $detail->restore();
            }
        }
    }

    public function restorePayments()
    {
        $payments = $this->payments()->onlyTrashed()->get();

        if ($payments->count() > 0) {
            foreach ($payments as $payment) {
                $payment->restore();
            }
        }
    }

    public function countTotal()
    {
        $total = 0;
        foreach ($this->details as $detail) {
            $total += $detail->getTotalPurchasePrice();
        }

        return $total;
    }

    public function countTotalSell()
    {
        $total = 0;
        foreach ($this->details as $detail) {
            $total += $detail->getTotalSellPrice();
        }

        return $total;
    }
}
