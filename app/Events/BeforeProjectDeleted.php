<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Project;

class BeforeProjectDeleted
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $project;
    public function __construct(Project $project)
    {
        $this->project = $project;
    }
}
