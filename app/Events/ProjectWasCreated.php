<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use App\Project;

class ProjectWasCreated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $project;
    public $purchases;
    public function __construct(Project $project, $purchases)
    {
        $this->project = $project;
        $this->purchases = $purchases;
    }
}
