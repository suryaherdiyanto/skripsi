<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseDetail extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'qty',
        'item_id',
        'purchase_id'
    ];

    private $statusText = [
        '0' => 'Belum Dibayar',
        '1' => 'Dibayar Sebagian',
        '2' => 'Lunas'
    ];

    public function purchase()
    {
        return $this->belongsTo(Purchase::class);
    }

    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    public function getTotalPurchasePrice()
    {
        return $this->item->purchase_price * $this->qty;
    }

    public function getTotalSellPrice()
    {
        return $this->item->sell_price * $this->qty;
    }

    public function totalPurchaseFormat()
    {
        return 'Rp. ' . number_format($this->getTotalPurchasePrice());
    }

    public function totalSellFormat()
    {
        return 'Rp. ' . number_format($this->getTotalSellPrice());
    }

    public function getStatusText($status)
    {
        return $this->statusText[$status];
    }
}
