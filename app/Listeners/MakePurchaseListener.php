<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ProjectWasCreated;

class MakePurchaseListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ProjectWasCreated $event)
    {
        $purchase = $event->project->purchases()->create([
            'date' => now()->format('Y-m-d'),
            'purchase_no' => getPurchaseNo($event->project),
            'total' => 0,
            'total_paid' => 0,
            'status' => 0
        ]);

        $data = [];
        foreach ($event->purchases as $key => $item) {
            array_push($data, ['item_id' => $item['item_id'], 'qty' => intval($item['qty']), 'price' => floatval($item['price'])]);
        }

        $purchase->details()->createMany($data);
    }
}
