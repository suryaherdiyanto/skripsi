<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\BeforeProjectDeleted;

class ClearPurchaseAndPayment
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(BeforeProjectDeleted $event)
    {
        if ($event->project->purchases) {
            $event->project->clearPurchases();
        }

        if ($event->project->payments) {
            $event->project->clearPayments();
        }
    }
}
