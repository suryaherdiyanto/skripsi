<?php

function getItemCode() {
    return 'BRG-' . randomLetters() . '-' . randomNumbers();
}

function getProjectNo() {
    $latest = App\Project::withTrashed()->orderBy('id', 'DESC')->first()->id ?? 0;
    return 'PRJ-' . ($latest + 1);
}

function getPurchaseNo($project) {
    $now = now();
    $latest = $project->purchases()->withTrashed()->orderBy('id', 'DESC')->first()->id ?? 0;

    return 'INV-' . now()->format('mdy') . '-' . $project->id . '-' . ($latest + 1);
}

function getPaymentNo($model) {
    $now = now();
    $latest = $model->payments()->withTrashed()->orderBy('id', 'DESC')->first()->id ?? 0;

    return 'PYT-' . now()->format('mdy') . '-' . $model->id . '-' . ($latest + 1);
}

function randomLetters($length = 3)
{
    $letters = 'abcfefghijklmnopqrstuvwxyz';

    return strtoupper(substr(str_shuffle($letters), 0, $length));
}

function randomNumbers($length = 3)
{
    $numbers = '1234567890';

    return substr(str_shuffle($numbers), 0, $length);
}