<?php

namespace App\Observers;

use App\Project;

class ProjectObserver {

    public function created(Project $project)
    {
        if (request()->has('purchases')) {
            
            $purchase = $project->purchases()->create([
                'date' => now()->format('Y-m-d'),
                'purchase_no' => getPurchaseNo($project),
                'total' => 0,
                'total_paid' => 0,
                'status' => 0
            ]);
                
            $purchases = request()->get('purchases');
            $data = [];
            foreach ($purchases as $key => $item) {
                if ($item['item_id'] > 0) {
                    # code...
                    array_push($data, ['item_id' => $item['item_id'], 'qty' => intval($item['qty'])]);
                }
            }
    
            $purchase->details()->createMany($data);
        }
    }

    public function deleted(Project $project)
    {
        $project->clearPurchases();

        $project->clearPayments();
    }

    public function restored(Project $project)
    {
        $project->restorePayments();

        $project->restorePurchases();
    }

}