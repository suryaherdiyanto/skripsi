<?php

namespace App\Observers;

use App\Purchase;

class PurchaseObserver
{
    private function updateProjectStatusPaid(&$project)
    {
        if (intval($project->total_paid) > 0 && intval($project->total_payment) == intval($project->total_paid)) {
            $project->payment_status = 2;
        } else if (intval($project->total_paid) > 0 && intval($project->total_paid) < intval($project->total_payment)) {
            $project->payment_status = 1;
        } else {
            $project->payment_status = 0;
        }
    }

    private function calculateProjectCost(&$purchase)
    {
        $project = $purchase->project;

        $project->total_cost = $project->totalPurchase();

        $project->total_payment = $project->totalCustomerPay();

        $this->updateProjectStatusPaid($project);
        
        $project->save();
    }

    private function subProjectCost(&$purchase)
    {
        
        $project = $purchase->project;

        if ($project) {
            $project->total_cost -= $purchase->countTotal();
    
            $project->total_payment -= $purchase->countTotalSell();
    
            $this->updateProjectStatusPaid($project);
            $project->save();
            
        }



    }

    public function created(Purchase $purchase)
    {
        $this->calculateProjectCost($purchase);
    }

    public function saved(Purchase $purchase)
    {
        $this->calculateProjectCost($purchase);
    }

    public function updated(Purchase $purchase)
    {
        $this->calculateProjectCost($purchase);
    }

    public function deleted(Purchase $purchase)
    {
        
        $this->subProjectCost($purchase);

        $purchase->clearDetails();

        $purchase->clearPayments();
    }

    public function restored(Purchase $purchase)
    {
        
        $purchase->restoreDetails();
        
        $purchase->restorePayments();

        $this->calculateProjectCost($purchase);

    }
}
