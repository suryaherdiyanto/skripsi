<?php

namespace App\Observers;

use App\PurchasePayment;

class PurchasePaymentObserver {

    private function addPurchaseTotal(&$payment)
    {
        $purchase = $payment->purchase;
        $totalPayment = $purchase->totalPayment();

        $purchase->total_paid = $totalPayment;

        if (intval($payment->total) === intval($purchase->total)) {
            $purchase->status = 2;
        }

        if (intval($totalPayment) == intval($purchase->total) ) {
            $purchase->status = 2;
        }

        if (intval($totalPayment) < intval($purchase->total)) {
           $purchase->status = 1;
        }

        $purchase->save();
    }

    private function subPurchaseTotal(&$payment)
    {
        $purchase = $payment->purchase;

        $purchase->total_paid -= $payment->total;

        if (intval($purchase->total_paid) < intval($purchase->total)) {
           $purchase->status = 1;
        }

        if (intval($purchase->total) === 0) {
            $purchase->status = 0;
        }

        $purchase->save();
    }
    

    public function created(PurchasePayment $payment)
    {
        $this->addPurchaseTotal($payment);
    }

    public function updated(PurchasePayment $payment)
    {
        $this->addPurchaseTotal($payment);
    }

    public function deleted(PurchasePayment $payment)
    {
        $this->subPurchaseTotal($payment);
    }

    public function restored(PurchasePayment $payment)
    {
        $this->addPurchaseTotal($payment);
    }

}