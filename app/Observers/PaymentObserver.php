<?php

namespace App\Observers;
use App\Payment;

class PaymentObserver
{
    private function calculateProjectPayment(Payment $payment)
    {
        $project = $payment->project;
        $totalPayment = $project->totalPayment();

        $project->total_paid = $totalPayment;

        if (intval($payment->total) >= intval($project->total_cost)) {
            $project->payment_status = 2;
        } else if (intval($project->total_paid) >= intval($project->total_cost) ) {
            $project->payment_status = 2;
        } else if (intval($project->total_paid) < intval($project->total_cost)) {
            $project->payment_status = 1;
        } else {
            $project->payment_status = 0;
        }

        $project->save();
    }

    private function subProjectPayment(Payment $payment)
    {
        $project = $payment->project;
        $totalPayment = $project->totalPayment();

        $project->total_paid -= $payment->total;

        if (intval($project->total_paid) < intval($project->total_cost)) {
            $project->payment_status = 1;
        } else {
            $project->payment_status = 0;
        }

        $project->save();
    }

    public function created(Payment $payment)
    {
        $this->calculateProjectPayment($payment);
    }

    public function updated(Payment $payment)
    {
        $this->calculateProjectPayment($payment);
    }

    public function deleted(Payment $payment)
    {
        $this->subProjectPayment($payment);
    }

    public function restored(Payment $payment)
    {
        $this->calculateProjectPayment($payment);
    }
}
