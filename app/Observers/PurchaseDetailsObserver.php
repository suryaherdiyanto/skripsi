<?php

namespace App\Observers;

use App\PurchaseDetail;
use App\Purchase;

class PurchaseDetailsObserver
{
    //

    private function calculatePurchaseDetails(&$purchase)
    {
        $purchase->total = $purchase->countTotal();

        $purchase->save();
    }

    public function created(PurchaseDetail $detail)
    {
        $this->calculatePurchaseDetails($detail->purchase);
    }

    public function saved(PurchaseDetail $detail)
    {
        $this->calculatePurchaseDetails($detail->purchase);
    }

    public function updated(PurchaseDetail $detail)
    {
        $this->calculatePurchaseDetails($detail->purchase);
    }

    public function deleted(PurchaseDetail $detail)
    {
        if ($detail->purchase) {
            $this->calculatePurchaseDetails($detail->purchase);
        }
    }

    public function restored(PurchaseDetail $detail)
    {
        $this->calculatePurchaseDetails($detail->purchase);
    }
}
