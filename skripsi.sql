-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
<<<<<<< HEAD
-- Host: localhost    Database: sie_pt_marga
-- ------------------------------------------------------
-- Server version	5.7.29
=======
-- Host: mysql
-- Generation Time: May 05, 2020 at 04:10 AM
-- Server version: 5.7.29
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

>>>>>>> 01f14d6d6c34e1da5a97a39cd702f857ddab1b4a

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'PT.PLN (Persero) Unit Wilayah Bali & Nusa Tenggara','pln123@pln.co.id','Pierre Yost MD','(0361) – 221960','Jl. Letda Tantular No.1',NULL,'2020-02-25 12:29:29','2020-03-29 12:08:57'),(2,'Pengeluaran Dinas Pertambangan dan Energi Provinsi NTB','desdem@ntbprov.go.id','Kiel Armstrong','(0370) 621356','JL.Majapahit No.40 Mataram NTB',NULL,'2020-02-25 12:29:29','2020-02-25 13:11:11'),(3,'Perusahaan Daerah Air Minum (PDAM) Tirta Manguntama','info@pdambadung.id','Adrianna Waters','0361-421-845','Jl. Bedahulu No. 3, Denpasar',NULL,'2020-02-25 12:29:29','2020-02-25 13:16:18'),(4,'PT. Keranjang','info@thekeranjangbali.com','Miss Viola Dickinson II','(0361) 4755575','Jl. By Pass Ngurah Rai No.97, Kuta',NULL,'2020-02-25 12:29:29','2020-02-25 13:18:27'),(5,'PT. Charoen Pookhand','investor.relations@cp.co.id','Dr. Deon Lebsack III','(021) 6919999','Jl. Gatot Subroto No.330X, Kesiman Petilan Denpasar Timur',NULL,'2020-02-25 12:29:29','2020-02-25 13:23:45'),(6,'PT. Universal Mas',NULL,'','(0361) 3700659','Seminyak Badung Bali',NULL,'2020-02-25 12:29:29','2020-02-25 13:33:05'),(7,'Carter, Schuster and Wolf','kenyon.johnson@jones.com','Macy Bayer','1-561-963-5432','8591 Tina Parkways\nSouth Napoleon, NY 23494-9824','2020-02-25 13:33:10','2020-02-25 12:29:29','2020-02-25 13:33:10'),(8,'Lindgren-Kohler','kallie.jast@hotmail.com','Mariane Wilderman','(213) 410-4618 x8620','26201 McDermott Drive Suite 430\nHodkiewiczhaven, MI 41713','2020-02-25 13:33:14','2020-02-25 12:29:29','2020-02-25 13:33:14'),(9,'Bins LLC','ddenesik@mertz.biz','Kennedi Turcotte','664.751.8049','261 Carroll Drive Suite 514\nMyraberg, MA 33535-4203','2020-02-25 13:33:17','2020-02-25 12:29:29','2020-02-25 13:33:17'),(10,'Langworth-Barton','fglover@yahoo.com','Lily Oberbrunner','692.495.9019','90432 Yundt Row Apt. 877\nDillanmouth, AL 26337-9458','2020-02-25 13:33:20','2020-02-25 12:29:29','2020-02-25 13:33:20');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `items` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_price` int(11) NOT NULL,
  `purchase_price` int(11) NOT NULL,
  `qty_type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `supplier_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `items_supplier_id_foreign` (`supplier_id`),
  CONSTRAINT `items_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `items`
--

LOCK TABLES `items` WRITE;
/*!40000 ALTER TABLE `items` DISABLE KEYS */;
INSERT INTO `items` VALUES (1,'BRG-OQF-809','Baut 16 x 50 mm²',4000,3500,'PCS','Voluptatum cupiditate quam asperiores ut. Voluptate voluptas qui et non sit.\n\nNostrum quasi dolor qui corporis. Quisquam dignissimos expedita odio voluptatum facere sit ullam.',5,NULL,'2020-02-25 12:29:29','2020-02-26 13:25:54'),(2,'BRG-GQH-869','Beton Block',42000,39500,'PCS','Qui asperiores consequatur velit vero. Ut non sed quis ut. Id voluptas architecto quia dolores et suscipit. Ipsum explicabo laborum doloremque aspernatur.\n\nA cupiditate quo voluptatum harum iure at sit. Velit est voluptas explicabo. Cupiditate cumque et nihil omnis deleniti. Repellat recusandae consequatur et rerum alias nesciunt sunt autem.',6,NULL,'2020-02-25 12:29:29','2020-02-26 13:31:53'),(3,'BRG-SNJ-680','Begel U. 3\"',12000,9000,'PCS','Fugit iusto in deserunt aut rerum qui non. Aspernatur alias eligendi ipsam. Est mollitia dicta voluptas aspernatur. Odio dicta vitae consequatur provident ut.\n\nOmnis et fugit quaerat architecto nulla architecto et qui. Ut eveniet libero eaque sit ullam.',1,NULL,'2020-02-25 12:29:30','2020-02-26 13:35:05'),(4,'BRG-ROM-672','CCO 35 - 70',12000,8000,'PCS','Et est expedita natus incidunt incidunt amet dolorem. Nesciunt quisquam voluptatum ratione soluta cumque et sequi. Est amet voluptates labore. Voluptates placeat voluptas nulla perferendis quam qui at dolorem.\n\nLaudantium voluptates omnis quas tenetur soluta odio. Consequatur dolorem non qui eos illo. Laborum officiis et amet recusandae eius. Omnis nobis est fugit quia sit assumenda.',7,NULL,'2020-02-25 12:29:30','2020-02-26 13:34:49'),(5,'BRG-VRF-957','CCO 16-35',8500,6500,'PCS','Itaque dolorem eum eum eaque sit qui fugiat. Possimus ratione voluptatem reiciendis et tempora. Accusamus assumenda consequatur asperiores aut.\n\nQuas at asperiores veniam nobis animi dolor est. Iusto delectus nobis in excepturi ex consequatur accusamus. In error atque inventore dolorem. Molestias tenetur autem officia aut odio.',7,NULL,'2020-02-25 12:29:30','2020-02-26 13:36:19'),(6,'BRG-UVE-456','CCO 8 T 8',23000,20000,'PCS','Et et blanditiis dolorem. Eaque placeat sit et eum nihil. Et suscipit vero iste porro odio omnis aliquid. Earum inventore voluptatem adipisci rem est consequuntur aut.\n\nVeritatis magni error quo voluptas. Aut et amet voluptas nisi qui. Doloremque nulla alias veritatis suscipit dolor.',7,NULL,'2020-02-25 12:29:30','2020-02-26 13:37:29'),(7,'BRG-RIP-879','Kabel AAAC/S 1 x 150 mm²',26000,24500,'METER','Illum nemo cum alias totam libero eum. Cum odio sed nulla veritatis odit. Natus sequi dolorem a. Rerum fuga at nisi natus est velit dicta.\n\nVel enim omnis expedita. In doloremque tempore voluptatem aperiam. Quasi nostrum mollitia aspernatur inventore excepturi fugit. Reiciendis at delectus iusto ut hic vel ullam. Culpa ratione suscipit assumenda quae.',2,NULL,'2020-02-25 12:29:30','2020-02-26 13:42:06'),(8,'BRG-KFQ-437','Kabel AAAC/S 1 x 70 mm²',14500,13000,'METER','Blanditiis ut illum odio ut. Totam debitis minima adipisci non sunt atque aut. Voluptatem dolorem qui et ea autem.\n\nFugiat ullam rerum rem et quae. Tempore ducimus ex quisquam voluptas rerum eum perspiciatis non. Consequatur delectus molestias voluptas ea autem. Ad unde aliquid quis et debitis nisi.',2,NULL,'2020-02-25 12:29:30','2020-02-26 13:43:09'),(9,'BRG-QOG-239','Kabel LVTC. 3 x 35 + 1x 25 mm²',24000,21780,'PCS','In voluptatum aperiam in hic. Reprehenderit est sit et et provident error. Et nobis rerum qui debitis commodi.\n\nSit non sit veniam rem et fugit error necessitatibus. Optio vel ad ut error nobis. Possimus vitae et alias laboriosam voluptatem nemo voluptatibus. Voluptas ab vitae quis ullam fugit sunt autem. Sunt eum aut corporis omnis quia.',2,NULL,'2020-02-25 12:29:30','2020-02-26 13:46:15'),(10,'BRG-RUX-182','Kabel  NYA 1 x 35 mm²',36500,34845,'METER','Illo inventore quibusdam molestiae earum sit. Nisi velit et magnam sit doloribus. Reiciendis quidem ea veniam perferendis blanditiis.\n\nNatus rerum sit hic qui consectetur voluptatibus doloribus. Eum aut omnis est consequatur sed est. Quisquam consequatur rerum aut qui consequatur quibusdam.',2,NULL,'2020-02-25 12:29:30','2020-02-26 13:47:53'),(11,'BRG-JWA-614','Klem begel 8 Inch',34000,30400,'PCS',NULL,3,NULL,'2020-02-26 13:51:09','2020-02-26 13:51:09'),(12,'BRG-VLF-208','Double Top Ties AAAC/S 150 mm²',215000,191700,'PCS',NULL,1,NULL,'2020-02-26 13:53:12','2020-02-26 13:53:12'),(13,'BRG-XIE-451','Kabel NYAF 1 x 35 mm²',41500,38809,'METER',NULL,2,NULL,'2020-02-26 13:54:52','2020-02-26 13:54:52'),(14,'BRG-OKF-652','Kabel NYY 1x 50 mm²',65000,63000,'METER',NULL,3,NULL,'2020-02-26 13:58:01','2020-02-26 13:58:01'),(15,'BRG-ONS-850','Kabel NYY 1x 70 mm²',85000,82000,'METER',NULL,3,NULL,'2020-02-26 13:58:28','2020-02-26 13:58:28'),(16,'BRG-BQY-648','Kabel NYY 4x 70 mm²',305000,290809,'METER',NULL,3,NULL,'2020-02-26 13:59:28','2020-02-26 14:01:52'),(17,'BRG-QCY-704','Isolator Tumpu 20 Kv',107500,203500,'UNIT',NULL,1,NULL,'2020-02-26 14:03:13','2020-02-26 14:03:13'),(18,'BRG-MFW-208','Isolator tarik (hang)',330000,325000,'UNIT',NULL,1,NULL,'2020-02-26 14:04:31','2020-02-26 14:04:31'),(19,'BRG-GYQ-671','Cut Out 24 kV Polymer dan Viker',1050000,918500,'UNIT',NULL,3,NULL,'2020-02-26 14:06:02','2020-02-26 14:06:02'),(20,'BRG-SUG-642','Cut Out',870000,825000,'UNIT',NULL,3,NULL,'2020-02-26 14:07:09','2020-02-26 14:07:09'),(21,'BRG-MFT-692','Panel TR 120x80x40 cm',8650000,8350000,'UNIT',NULL,3,NULL,'2020-02-29 04:48:12','2020-02-29 04:48:12'),(22,'BRG-FUH-216','Stabiliser 33  KvA',40000000,37000000,'UNIT',NULL,2,NULL,'2020-02-29 04:51:02','2020-02-29 04:51:02'),(23,'BRG-GSP-243','Kontramast 36 KN TM ( SCS )',850000,650000,'UNIT',NULL,2,NULL,'2020-02-29 04:53:47','2020-02-29 04:53:47');
/*!40000 ALTER TABLE `items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2013_11_16_122707_create_roles_table',1),(2,'2013_11_16_122813_create_permissions_table',1),(3,'2014_10_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_resets_table',1),(5,'2019_08_19_000000_create_failed_jobs_table',1),(6,'2019_11_16_123102_create_role_permission_table_table',1),(7,'2019_11_16_123916_create_suppliers_table',1),(8,'2019_11_16_123924_create_items_table',1),(9,'2019_11_16_124526_create_customers_table',1),(10,'2019_11_16_124949_create_projects_table',1),(11,'2019_11_16_125009_create_payments_table',1),(12,'2019_11_16_125022_create_purchases_table',1),(13,'2019_11_16_125037_create_purchase_details_table',1),(14,'2019_11_16_130918_add_deleted_at_to_payments_table',1),(15,'2019_11_16_131006_add_deleted_at_to_purchases_table',1),(16,'2019_11_16_131211_add_deleted_at_to_purchase_details_table',1),(17,'2019_11_22_113753_add_deleted_at_field_to_users_table',1),(18,'2019_11_23_050031_add_name_field_to_projects_table',1),(19,'2019_12_13_061843_create_purchase_payments_table',1),(20,'2019_12_15_134853_add_deleted_at_field_to_payments_table',1),(21,'2019_12_16_220220_add_field_to_purchases',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `project_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `payments_payment_no_unique` (`payment_no`),
  KEY `payments_project_id_foreign` (`project_id`),
  CONSTRAINT `payments_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'PYT-022920-1-1','2019-08-12',34490600,1,'2020-02-29 06:43:15','2020-02-29 06:53:24',NULL),(2,'PYT-022920-1-2','2019-10-18',30829400,1,'2020-02-29 06:43:59','2020-02-29 06:43:59',NULL),(3,'PYT-022920-2-1','2019-12-26',31500000,2,'2020-02-29 07:25:39','2020-02-29 07:27:35',NULL),(4,'PYT-022920-2-4','2019-12-30',31135000,2,'2020-02-29 07:28:13','2020-02-29 07:28:13',NULL),(5,'PYT-022920-3-1','2019-03-25',15000000,3,'2020-02-29 07:33:16','2020-02-29 07:33:16',NULL),(6,'PYT-022920-3-6','2019-04-04',20020000,3,'2020-02-29 07:37:29','2020-02-29 07:37:29',NULL),(7,'PYT-022920-4-1','2019-06-17',30000000,4,'2020-02-29 07:44:10','2020-02-29 07:44:10',NULL),(8,'PYT-022920-4-8','2019-06-25',30000000,4,'2020-02-29 07:52:00','2020-02-29 07:52:00',NULL),(9,'PYT-022920-4-9','2019-07-03',71800000,4,'2020-02-29 07:56:52','2020-02-29 07:56:52',NULL),(10,'PYT-032920-5-1','2019-09-12',25000000,5,'2020-03-29 11:32:27','2020-03-29 11:32:27',NULL),(11,'PYT-032920-5-11','2019-09-30',50000000,5,'2020-03-29 11:36:41','2020-03-29 11:36:41',NULL),(12,'PYT-032920-5-12','2019-10-10',45000000,5,'2020-03-29 11:49:04','2020-03-29 11:49:04',NULL),(13,'PYT-032920-5-13','2019-11-20',20000000,5,'2020-03-29 11:52:37','2020-03-29 11:52:37',NULL),(14,'PYT-032920-5-14','2019-12-05',21975000,5,'2020-03-29 11:53:19','2020-03-29 11:53:19',NULL),(15,'PYT-040520-3-7','2019-05-01',25000000,3,'2020-04-05 12:06:07','2020-04-05 12:06:07',NULL),(16,'PYT-040520-3-16','2019-05-07',21125000,3,'2020-04-05 12:07:30','2020-04-05 12:08:05',NULL);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'list user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(2,'show user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(3,'store user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(4,'update user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(5,'delete user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(6,'force-delete user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(7,'restore user',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(8,'list item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(9,'show item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(10,'store item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(11,'update item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(12,'delete item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(13,'force-delete item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(14,'restore item',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(15,'list customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(16,'show customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(17,'store customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(18,'update customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(19,'delete customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(20,'force-delete customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(21,'restore customer',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(22,'list supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(23,'show supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(24,'store supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(25,'update supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(26,'delete supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(27,'force-delete supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(28,'restore supplier',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(29,'list project',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(30,'show project',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(31,'store project',NULL,'2020-02-25 12:29:28','2020-02-25 12:29:28'),(32,'update project',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(33,'delete project',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(34,'force-delete project',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(35,'restore project',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(36,'list role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(37,'show role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(38,'store role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(39,'update role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(40,'delete role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(41,'force-delete role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(42,'restore role',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(43,'list purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(44,'show purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(45,'store purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(46,'update purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(47,'delete purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(48,'force-delete purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(49,'restore purchase',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(50,'list payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(51,'show payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(52,'store payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(53,'update payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(54,'delete payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(55,'force-delete payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(56,'restore payment',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(57,'view report',NULL,'2020-02-25 20:36:43','2020-02-25 20:36:51');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `payment_status` tinyint(4) NOT NULL DEFAULT '0',
  `total_cost` int(11) NOT NULL DEFAULT '0',
  `total_payment` int(11) NOT NULL DEFAULT '0',
  `total_paid` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `customer_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
<<<<<<< HEAD
=======
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `project_no`, `start_date`, `payment_status`, `total_cost`, `total_payment`, `total_paid`, `description`, `customer_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'Pengadaan dan Pemasangan Cubicle 690 kVA', 'PRJ-1', '2019-10-13', 2, 60829400, 65320000, 65320000, 'Pengadaan dan Pemasangan Cubicle 690 kVA Lengkap dengan Bangunan Gardu di IPP Ungasan Perusahaan Daerah Air Minum (PDAM) Tirta Manguntama', 3, NULL, '2020-02-25 12:29:30', '2020-02-29 06:53:24'),
(2, 'Perluasan Jaringan Perubahan Daya Kubikel', 'PRJ-2', '2019-12-24', 2, 58337250, 62635000, 62635000, 'Perluasan Jaringan Perubahan Daya Kubikel a/n PT. Keranjang – Bypass Ngurah Rai (Kuta)', 4, NULL, '2020-02-25 12:29:30', '2020-02-29 07:28:13'),
(3, 'Perluasan Jaringan Perubahan Daya Kubikel', 'PRJ-3', '2019-03-25', 2, 76417150, 81145000, 81145000, 'Perluasan Jaringan Perubahan Daya Kubikel', 5, NULL, '2020-02-25 12:29:30', '2020-04-05 12:07:30'),
(4, 'Perluasan Jaringan Perubahan Daya Kubikel', 'PRJ-4', '2019-06-19', 2, 123031250, 131800000, 131800000, 'Perluasan Jaringan Perubahan Daya Kubikel a/n PT. Universal Mas (Hotel) – Petitenget', 6, NULL, '2020-02-25 12:29:30', '2020-02-29 07:56:52'),
(5, 'Pemasangan Tambahan Gardu Listrik PLN - Sanur', 'PRJ-5', '2019-09-10', 2, 150830400, 161975000, 161975000, 'Penambahan Gardu Listrik PLN Daerah Sanu', 1, NULL, '2020-02-25 12:29:30', '2020-03-29 11:53:19'),
(6, 'sunt autem', 'PRJ-6', '2008-10-21', 0, 0, 0, 0, 'Ea officia qui et ut. Et minima omnis repellat enim. Repellat vel doloremque sit optio itaque velit autem.\n\nEos aut reprehenderit facere numquam similique. Explicabo quia non delectus omnis. Provident earum aut fuga et nemo quia.\n\nEt iste in aut quasi qui. Quia hic et et repudiandae. Dignissimos animi nobis fugit. Excepturi delectus praesentium tempore sapiente sit optio.', 4, NULL, '2020-02-25 12:29:30', '2020-02-25 12:29:30'),
(7, 'sed error', 'PRJ-7', '2013-12-26', 0, 0, 0, 0, 'Natus recusandae sint ullam omnis ut sunt neque. Consequatur quo velit laudantium nisi libero labore. Laborum veniam illum a excepturi deserunt corporis rem. Vel quibusdam rerum at aut.\n\nQuos itaque nostrum possimus alias quam. Esse et soluta maiores quaerat excepturi laudantium culpa. Laboriosam hic quo maxime earum. Eveniet quod qui tenetur expedita pariatur nemo. Molestiae autem officia eligendi porro debitis impedit inventore nesciunt.\n\nEaque et nihil veritatis. Dolor accusantium et natus aut aut. Libero eaque officiis voluptas illum ratione dolorem velit aspernatur.', 2, NULL, '2020-02-25 12:29:30', '2020-02-25 12:29:30'),
(8, 'neque eum', 'PRJ-8', '1986-08-04', 0, 0, 0, 0, 'Eos error est sunt iste. Quos sit dolorem veritatis ipsum quasi et possimus. Cumque ut explicabo corporis quisquam.\n\nAb consequatur sit consectetur omnis sint. Debitis vero corrupti ullam. Et corporis ullam reiciendis impedit tempore. Ea non vero nam illo omnis et.\n\nVelit minus accusamus accusantium commodi optio voluptatem numquam quod. Eveniet in qui ex dolorum rem rerum. Occaecati et maiores veritatis deserunt accusamus. Sed enim rerum illo fuga facere maxime reiciendis neque. Vitae saepe fugit rem recusandae eos illo ut.', 6, NULL, '2020-02-25 12:29:30', '2020-02-25 12:29:30'),
(9, 'tempore voluptatem', 'PRJ-9', '2003-05-25', 0, 0, 0, 0, 'Est delectus tempore animi adipisci. Consequatur laudantium laboriosam quisquam quaerat sunt. Mollitia nihil illum iure architecto ducimus. Necessitatibus laborum doloremque enim eum consequuntur.\n\nEst in temporibus aperiam aperiam. Amet aut deserunt blanditiis aliquam qui id. Quae inventore adipisci consectetur sequi. Sequi dicta laboriosam ullam distinctio atque.\n\nMagni eos quo id accusantium dolorum. Sapiente magnam quia aut ipsa non. Provident ut qui aspernatur cupiditate quaerat. Sit est et cum repudiandae qui sit dolores.', 4, NULL, '2020-02-25 12:29:30', '2020-02-25 12:29:30'),
(10, 'possimus harum', 'PRJ-10', '1972-06-24', 0, 0, 0, 0, 'Velit cupiditate sapiente itaque odio repellendus aut. Et aliquam iste dolor. Maiores molestiae quibusdam omnis rerum eveniet quo inventore. Esse autem qui sint eum.\n\nNobis quisquam ea eligendi occaecati necessitatibus. Sed distinctio nihil ut mollitia.\n\nEst quae quae doloribus adipisci voluptatem veniam voluptatum. Impedit et consequatur culpa.', 4, NULL, '2020-02-25 12:29:30', '2020-02-25 12:29:30'),
(12, 'qwer', 'PRJ-12', '2020-04-14', 0, 2900000, 3074500, 0, 'asdfff', 4, NULL, '2020-04-14 09:05:40', '2020-04-14 09:21:40'),
(13, 'asdf', 'PRJ-13', '2020-04-14', 0, 247500, 272500, 0, 'asdf', 1, NULL, '2020-04-14 09:12:14', '2020-04-14 09:12:14'),
(14, 'Maintenance Gardu', 'PRJ-14', '2020-05-06', 0, 685000, 770000, 0, NULL, 3, NULL, '2020-05-05 03:59:31', '2020-05-05 03:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `total_paid` double(25,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `project_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
>>>>>>> 01f14d6d6c34e1da5a97a39cd702f857ddab1b4a
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `projects_project_no_unique` (`project_no`),
  KEY `projects_customer_id_foreign` (`customer_id`),
  CONSTRAINT `projects_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

<<<<<<< HEAD
LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Pengadaan dan Pemasangan Cubicle 690 kVA','PRJ-1','2019-10-13',2,60829400,65320000,65320000,'Pengadaan dan Pemasangan Cubicle 690 kVA Lengkap dengan Bangunan Gardu di IPP Ungasan Perusahaan Daerah Air Minum (PDAM) Tirta Manguntama',3,NULL,'2020-02-25 12:29:30','2020-02-29 06:53:24'),(2,'Perluasan Jaringan Perubahan Daya Kubikel','PRJ-2','2019-12-24',2,58337250,62635000,62635000,'Perluasan Jaringan Perubahan Daya Kubikel a/n PT. Keranjang – Bypass Ngurah Rai (Kuta)',4,NULL,'2020-02-25 12:29:30','2020-02-29 07:28:13'),(3,'Perluasan Jaringan Perubahan Daya Kubikel','PRJ-3','2019-03-25',2,76417150,81145000,81145000,'Perluasan Jaringan Perubahan Daya Kubikel',5,NULL,'2020-02-25 12:29:30','2020-04-05 12:07:30'),(4,'Perluasan Jaringan Perubahan Daya Kubikel','PRJ-4','2019-06-19',2,123031250,131800000,131800000,'Perluasan Jaringan Perubahan Daya Kubikel a/n PT. Universal Mas (Hotel) – Petitenget',6,NULL,'2020-02-25 12:29:30','2020-02-29 07:56:52'),(5,'Pemasangan Tambahan Gardu Listrik PLN - Sanur','PRJ-5','2019-09-10',2,150830400,161975000,161975000,'Penambahan Gardu Listrik PLN Daerah Sanu',1,NULL,'2020-02-25 12:29:30','2020-03-29 11:53:19'),(6,'sunt autem','PRJ-6','2008-10-21',0,0,0,0,'Ea officia qui et ut. Et minima omnis repellat enim. Repellat vel doloremque sit optio itaque velit autem.\n\nEos aut reprehenderit facere numquam similique. Explicabo quia non delectus omnis. Provident earum aut fuga et nemo quia.\n\nEt iste in aut quasi qui. Quia hic et et repudiandae. Dignissimos animi nobis fugit. Excepturi delectus praesentium tempore sapiente sit optio.',4,NULL,'2020-02-25 12:29:30','2020-02-25 12:29:30'),(7,'sed error','PRJ-7','2013-12-26',0,0,0,0,'Natus recusandae sint ullam omnis ut sunt neque. Consequatur quo velit laudantium nisi libero labore. Laborum veniam illum a excepturi deserunt corporis rem. Vel quibusdam rerum at aut.\n\nQuos itaque nostrum possimus alias quam. Esse et soluta maiores quaerat excepturi laudantium culpa. Laboriosam hic quo maxime earum. Eveniet quod qui tenetur expedita pariatur nemo. Molestiae autem officia eligendi porro debitis impedit inventore nesciunt.\n\nEaque et nihil veritatis. Dolor accusantium et natus aut aut. Libero eaque officiis voluptas illum ratione dolorem velit aspernatur.',2,NULL,'2020-02-25 12:29:30','2020-02-25 12:29:30'),(8,'neque eum','PRJ-8','1986-08-04',0,0,0,0,'Eos error est sunt iste. Quos sit dolorem veritatis ipsum quasi et possimus. Cumque ut explicabo corporis quisquam.\n\nAb consequatur sit consectetur omnis sint. Debitis vero corrupti ullam. Et corporis ullam reiciendis impedit tempore. Ea non vero nam illo omnis et.\n\nVelit minus accusamus accusantium commodi optio voluptatem numquam quod. Eveniet in qui ex dolorum rem rerum. Occaecati et maiores veritatis deserunt accusamus. Sed enim rerum illo fuga facere maxime reiciendis neque. Vitae saepe fugit rem recusandae eos illo ut.',6,NULL,'2020-02-25 12:29:30','2020-02-25 12:29:30'),(9,'tempore voluptatem','PRJ-9','2003-05-25',0,0,0,0,'Est delectus tempore animi adipisci. Consequatur laudantium laboriosam quisquam quaerat sunt. Mollitia nihil illum iure architecto ducimus. Necessitatibus laborum doloremque enim eum consequuntur.\n\nEst in temporibus aperiam aperiam. Amet aut deserunt blanditiis aliquam qui id. Quae inventore adipisci consectetur sequi. Sequi dicta laboriosam ullam distinctio atque.\n\nMagni eos quo id accusantium dolorum. Sapiente magnam quia aut ipsa non. Provident ut qui aspernatur cupiditate quaerat. Sit est et cum repudiandae qui sit dolores.',4,NULL,'2020-02-25 12:29:30','2020-02-25 12:29:30'),(10,'possimus harum','PRJ-10','1972-06-24',0,0,0,0,'Velit cupiditate sapiente itaque odio repellendus aut. Et aliquam iste dolor. Maiores molestiae quibusdam omnis rerum eveniet quo inventore. Esse autem qui sint eum.\n\nNobis quisquam ea eligendi occaecati necessitatibus. Sed distinctio nihil ut mollitia.\n\nEst quae quae doloribus adipisci voluptatem veniam voluptatum. Impedit et consequatur culpa.',4,NULL,'2020-02-25 12:29:30','2020-02-25 12:29:30'),(12,'qwer','PRJ-12','2020-04-14',0,2900000,3074500,0,'asdfff',4,NULL,'2020-04-14 09:05:40','2020-04-14 09:21:40'),(13,'asdf','PRJ-13','2020-04-14',0,247500,272500,0,'asdf',1,NULL,'2020-04-14 09:12:14','2020-04-14 09:12:14');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;
=======
INSERT INTO `purchases` (`id`, `purchase_no`, `date`, `total`, `total_paid`, `status`, `project_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'INV-022920-1-1', '2019-10-01', 7129400, 7129400.00, 2, 1, '2020-02-29 04:31:12', '2020-02-29 07:16:19', NULL),
(2, 'INV-022920-1-2', '2019-10-09', 53700000, 53700000.00, 2, 1, '2020-02-29 04:55:23', '2020-02-29 07:18:37', NULL),
(3, 'INV-022920-2-1', '2019-12-20', 58337250, 58337250.00, 2, 2, '2020-02-29 07:21:08', '2020-02-29 07:27:06', NULL),
(4, 'INV-022920-3-1', '2019-03-22', 15501250, 15501250.00, 2, 3, '2020-02-29 07:31:01', '2020-02-29 07:35:17', NULL),
(5, 'INV-022920-3-5', '2019-03-28', 18225450, 18225450.00, 2, 3, '2020-02-29 07:32:05', '2020-02-29 07:36:34', NULL),
(6, 'INV-022920-4-1', '2019-06-10', 123031250, 123031250.00, 2, 4, '2020-02-29 07:43:23', '2020-02-29 07:57:28', NULL),
(7, 'INV-032920-5-1', '2019-09-12', 93436500, 93436500.00, 2, 5, '2020-03-29 11:24:38', '2020-03-29 11:49:44', NULL),
(8, 'INV-032920-5-8', '2019-11-05', 57393900, 57393900.00, 2, 5, '2020-03-29 11:39:13', '2020-03-29 11:54:23', NULL),
(9, 'INV-040520-3-6', '2019-04-16', 42690450, 42690450.00, 2, 3, '2020-04-05 12:02:59', '2020-04-05 12:08:57', NULL),
(11, 'INV-041420-12-1', '2020-04-14', 19000, 0.00, 0, 12, '2020-04-14 09:05:40', '2020-04-14 09:05:40', NULL),
(12, 'INV-041420-13-1', '2020-04-14', 247500, 0.00, 0, 13, '2020-04-14 09:12:14', '2020-04-14 09:12:14', NULL),
(15, 'INV-041420-12-15', '2020-04-14', 297500, 0.00, 0, 12, '2020-04-14 09:20:39', '2020-04-14 09:20:39', NULL),
(16, 'INV-041420-12-16', '2020-04-14', 2583500, 0.00, 0, 12, '2020-04-14 09:21:40', '2020-04-14 09:21:40', NULL),
(17, 'INV-050520-14-1', '2020-05-05', 685000, 0.00, 0, 14, '2020-05-05 03:59:31', '2020-05-05 03:59:32', NULL);

-- --------------------------------------------------------
>>>>>>> 01f14d6d6c34e1da5a97a39cd702f857ddab1b4a

--
-- Table structure for table `purchase_details`
--

DROP TABLE IF EXISTS `purchase_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_details` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `qty` int(11) NOT NULL DEFAULT '0',
  `item_id` bigint(20) unsigned NOT NULL,
  `purchase_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `purchase_details_item_id_foreign` (`item_id`),
  KEY `purchase_details_purchase_id_foreign` (`purchase_id`),
  CONSTRAINT `purchase_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  CONSTRAINT `purchase_details_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_details`
--

<<<<<<< HEAD
LOCK TABLES `purchase_details` WRITE;
/*!40000 ALTER TABLE `purchase_details` DISABLE KEYS */;
INSERT INTO `purchase_details` VALUES (1,100,1,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(2,5,18,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(3,100,5,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(4,50,4,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(5,20,10,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(6,15,7,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(7,100,11,1,'2020-02-29 04:31:12','2020-02-29 04:31:12',NULL),(8,1,22,2,'2020-02-29 04:55:23','2020-02-29 04:55:23',NULL),(9,2,21,2,'2020-02-29 04:55:23','2020-02-29 04:55:23',NULL),(10,100,1,3,'2020-02-29 07:21:08','2020-02-29 07:21:08',NULL),(11,30,3,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(12,100,5,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(13,50,4,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(14,50,10,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(15,50,7,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(16,2,21,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(17,1,22,3,'2020-02-29 07:21:09','2020-02-29 07:21:09',NULL),(18,100,1,4,'2020-02-29 07:31:01','2020-02-29 07:31:01',NULL),(19,50,3,4,'2020-02-29 07:31:01','2020-02-29 07:31:01',NULL),(20,100,4,4,'2020-02-29 07:31:01','2020-02-29 07:31:01',NULL),(21,60,6,4,'2020-02-29 07:31:02','2020-02-29 07:31:02',NULL),(22,50,10,4,'2020-02-29 07:31:02','2020-02-29 07:31:02',NULL),(23,50,9,4,'2020-02-29 07:31:02','2020-02-29 07:31:02',NULL),(24,1,21,4,'2020-02-29 07:31:02','2020-02-29 07:31:02',NULL),(25,50,11,4,'2020-02-29 07:31:02','2020-02-29 07:31:02',NULL),(26,2,20,5,'2020-02-29 07:32:05','2020-02-29 07:32:05',NULL),(27,10,17,5,'2020-02-29 07:32:05','2020-02-29 07:32:05',NULL),(28,50,16,5,'2020-02-29 07:32:05','2020-02-29 07:32:05',NULL),(29,200,1,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(30,200,3,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(31,50,5,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(32,50,4,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(33,50,10,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(34,50,7,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(35,50,9,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(36,5,21,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(37,2,22,6,'2020-02-29 07:43:23','2020-02-29 07:43:23',NULL),(38,100,1,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(39,100,3,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(40,50,2,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(41,150,5,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(42,5,19,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(43,200,10,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(44,150,7,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(45,2,22,7,'2020-03-29 11:24:38','2020-03-29 11:24:38',NULL),(46,50,12,8,'2020-03-29 11:39:13','2020-03-29 11:39:13',NULL),(47,100,9,8,'2020-03-29 11:39:13','2020-03-29 11:39:13',NULL),(48,100,13,8,'2020-03-29 11:39:13','2020-03-29 11:39:13',NULL),(49,5,21,8,'2020-03-29 11:39:13','2020-03-29 11:39:13',NULL),(50,100,7,9,'2020-04-05 12:02:59','2020-04-05 12:02:59',NULL),(51,100,8,9,'2020-04-05 12:02:59','2020-04-05 12:02:59',NULL),(52,50,13,9,'2020-04-05 12:02:59','2020-04-05 12:02:59',NULL),(53,1,22,9,'2020-04-05 12:02:59','2020-04-05 12:02:59',NULL),(58,1,3,11,'2020-04-14 09:05:40','2020-04-14 09:05:40',NULL),(59,1,5,11,'2020-04-14 09:05:40','2020-04-14 09:05:40',NULL),(60,1,1,11,'2020-04-14 09:05:40','2020-04-14 09:05:40',NULL),(61,5,1,12,'2020-04-14 09:12:14','2020-04-14 09:12:14',NULL),(62,5,2,12,'2020-04-14 09:12:14','2020-04-14 09:12:14',NULL),(63,5,5,12,'2020-04-14 09:12:14','2020-04-14 09:12:14',NULL),(64,5,2,15,'2020-04-14 09:20:39','2020-04-14 09:20:39',NULL),(65,5,6,15,'2020-04-14 09:20:39','2020-04-14 09:20:39',NULL),(66,5,12,16,'2020-04-14 09:21:40','2020-04-14 09:21:40',NULL),(67,5,18,16,'2020-04-14 09:21:40','2020-04-14 09:21:40',NULL);
/*!40000 ALTER TABLE `purchase_details` ENABLE KEYS */;
UNLOCK TABLES;
=======
INSERT INTO `purchase_details` (`id`, `qty`, `item_id`, `purchase_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 100, 1, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(2, 5, 18, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(3, 100, 5, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(4, 50, 4, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(5, 20, 10, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(6, 15, 7, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(7, 100, 11, 1, '2020-02-29 04:31:12', '2020-02-29 04:31:12', NULL),
(8, 1, 22, 2, '2020-02-29 04:55:23', '2020-02-29 04:55:23', NULL),
(9, 2, 21, 2, '2020-02-29 04:55:23', '2020-02-29 04:55:23', NULL),
(10, 100, 1, 3, '2020-02-29 07:21:08', '2020-02-29 07:21:08', NULL),
(11, 30, 3, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(12, 100, 5, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(13, 50, 4, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(14, 50, 10, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(15, 50, 7, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(16, 2, 21, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(17, 1, 22, 3, '2020-02-29 07:21:09', '2020-02-29 07:21:09', NULL),
(18, 100, 1, 4, '2020-02-29 07:31:01', '2020-02-29 07:31:01', NULL),
(19, 50, 3, 4, '2020-02-29 07:31:01', '2020-02-29 07:31:01', NULL),
(20, 100, 4, 4, '2020-02-29 07:31:01', '2020-02-29 07:31:01', NULL),
(21, 60, 6, 4, '2020-02-29 07:31:02', '2020-02-29 07:31:02', NULL),
(22, 50, 10, 4, '2020-02-29 07:31:02', '2020-02-29 07:31:02', NULL),
(23, 50, 9, 4, '2020-02-29 07:31:02', '2020-02-29 07:31:02', NULL),
(24, 1, 21, 4, '2020-02-29 07:31:02', '2020-02-29 07:31:02', NULL),
(25, 50, 11, 4, '2020-02-29 07:31:02', '2020-02-29 07:31:02', NULL),
(26, 2, 20, 5, '2020-02-29 07:32:05', '2020-02-29 07:32:05', NULL),
(27, 10, 17, 5, '2020-02-29 07:32:05', '2020-02-29 07:32:05', NULL),
(28, 50, 16, 5, '2020-02-29 07:32:05', '2020-02-29 07:32:05', NULL),
(29, 200, 1, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(30, 200, 3, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(31, 50, 5, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(32, 50, 4, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(33, 50, 10, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(34, 50, 7, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(35, 50, 9, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(36, 5, 21, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(37, 2, 22, 6, '2020-02-29 07:43:23', '2020-02-29 07:43:23', NULL),
(38, 100, 1, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(39, 100, 3, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(40, 50, 2, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(41, 150, 5, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(42, 5, 19, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(43, 200, 10, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(44, 150, 7, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(45, 2, 22, 7, '2020-03-29 11:24:38', '2020-03-29 11:24:38', NULL),
(46, 50, 12, 8, '2020-03-29 11:39:13', '2020-03-29 11:39:13', NULL),
(47, 100, 9, 8, '2020-03-29 11:39:13', '2020-03-29 11:39:13', NULL),
(48, 100, 13, 8, '2020-03-29 11:39:13', '2020-03-29 11:39:13', NULL),
(49, 5, 21, 8, '2020-03-29 11:39:13', '2020-03-29 11:39:13', NULL),
(50, 100, 7, 9, '2020-04-05 12:02:59', '2020-04-05 12:02:59', NULL),
(51, 100, 8, 9, '2020-04-05 12:02:59', '2020-04-05 12:02:59', NULL),
(52, 50, 13, 9, '2020-04-05 12:02:59', '2020-04-05 12:02:59', NULL),
(53, 1, 22, 9, '2020-04-05 12:02:59', '2020-04-05 12:02:59', NULL),
(58, 1, 3, 11, '2020-04-14 09:05:40', '2020-04-14 09:05:40', NULL),
(59, 1, 5, 11, '2020-04-14 09:05:40', '2020-04-14 09:05:40', NULL),
(60, 1, 1, 11, '2020-04-14 09:05:40', '2020-04-14 09:05:40', NULL),
(61, 5, 1, 12, '2020-04-14 09:12:14', '2020-04-14 09:12:14', NULL),
(62, 5, 2, 12, '2020-04-14 09:12:14', '2020-04-14 09:12:14', NULL),
(63, 5, 5, 12, '2020-04-14 09:12:14', '2020-04-14 09:12:14', NULL),
(64, 5, 2, 15, '2020-04-14 09:20:39', '2020-04-14 09:20:39', NULL),
(65, 5, 6, 15, '2020-04-14 09:20:39', '2020-04-14 09:20:39', NULL),
(66, 5, 12, 16, '2020-04-14 09:21:40', '2020-04-14 09:21:40', NULL),
(67, 5, 18, 16, '2020-04-14 09:21:40', '2020-04-14 09:21:40', NULL),
(68, 10, 2, 17, '2020-05-05 03:59:31', '2020-05-05 03:59:31', NULL),
(69, 10, 3, 17, '2020-05-05 03:59:31', '2020-05-05 03:59:31', NULL),
(70, 10, 6, 17, '2020-05-05 03:59:32', '2020-05-05 03:59:32', NULL);

-- --------------------------------------------------------
>>>>>>> 01f14d6d6c34e1da5a97a39cd702f857ddab1b4a

--
-- Table structure for table `purchase_payments`
--

DROP TABLE IF EXISTS `purchase_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_payments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `payment_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `purchase_id` bigint(20) unsigned NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purchase_payments_payment_no_unique` (`payment_no`),
  KEY `purchase_payments_purchase_id_foreign` (`purchase_id`),
  CONSTRAINT `purchase_payments_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_payments`
--

LOCK TABLES `purchase_payments` WRITE;
/*!40000 ALTER TABLE `purchase_payments` DISABLE KEYS */;
INSERT INTO `purchase_payments` VALUES (1,'PYT-022920-1-1','2019-10-02',7129400,1,NULL,'2020-02-29 07:16:19','2020-02-29 07:16:19'),(2,'PYT-022920-2-1','2019-10-11',25000000,2,NULL,'2020-02-29 07:16:58','2020-02-29 07:16:58'),(3,'PYT-022920-2-3','2019-10-14',28700000,2,NULL,'2020-02-29 07:18:37','2020-02-29 07:18:37'),(4,'PYT-022920-3-1','2019-12-23',30000000,3,NULL,'2020-02-29 07:24:37','2020-02-29 07:24:37'),(5,'PYT-022920-3-5','2019-12-26',28337250,3,NULL,'2020-02-29 07:27:06','2020-02-29 07:27:06'),(6,'PYT-022920-4-1','2019-03-25',15501250,4,NULL,'2020-02-29 07:35:17','2020-02-29 07:35:17'),(7,'PYT-022920-5-1','2019-04-02',18225450,5,NULL,'2020-02-29 07:36:34','2020-02-29 07:36:34'),(8,'PYT-022920-6-1','2019-06-20',30000000,6,NULL,'2020-02-29 07:49:34','2020-02-29 07:49:34'),(9,'PYT-022920-6-9','2019-06-25',50000000,6,'2020-02-29 07:54:39','2020-02-29 07:50:30','2020-02-29 07:54:39'),(10,'PYT-022920-6-10','2019-06-27',50000000,6,NULL,'2020-02-29 07:55:54','2020-02-29 07:55:54'),(11,'PYT-022920-6-11','2019-07-05',43031250,6,NULL,'2020-02-29 07:57:28','2020-02-29 07:57:28'),(12,'PYT-032920-7-1','2019-09-16',30000000,7,NULL,'2020-03-29 11:31:26','2020-03-29 11:31:26'),(13,'PYT-032920-7-13','2019-09-25',35000000,7,NULL,'2020-03-29 11:35:46','2020-03-29 11:35:46'),(14,'PYT-032920-7-14','2019-10-15',28436500,7,NULL,'2020-03-29 11:49:44','2020-03-29 11:49:44'),(15,'PYT-032920-8-1','2019-11-08',15000000,8,NULL,'2020-03-29 11:51:43','2020-03-29 11:51:43'),(16,'PYT-032920-8-16','2019-12-10',25000000,8,NULL,'2020-03-29 11:53:56','2020-03-29 11:53:56'),(17,'PYT-032920-8-17','2020-01-03',17393900,8,NULL,'2020-03-29 11:54:23','2020-03-29 11:54:23'),(18,'PYT-040520-9-1','2019-04-19',20000000,9,NULL,'2020-04-05 12:04:54','2020-04-05 12:04:54'),(19,'PYT-040520-9-19','2019-05-10',22690450,9,NULL,'2020-04-05 12:08:57','2020-04-05 12:08:57');
/*!40000 ALTER TABLE `purchase_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_no` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `total` int(11) NOT NULL,
  `total_paid` double(25,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `project_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `purchases_purchase_no_unique` (`purchase_no`),
  KEY `purchases_project_id_foreign` (`project_id`),
  CONSTRAINT `purchases_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
INSERT INTO `purchases` VALUES (1,'INV-022920-1-1','2019-10-01',7129400,7129400.00,2,1,'2020-02-29 04:31:12','2020-02-29 07:16:19',NULL),(2,'INV-022920-1-2','2019-10-09',53700000,53700000.00,2,1,'2020-02-29 04:55:23','2020-02-29 07:18:37',NULL),(3,'INV-022920-2-1','2019-12-20',58337250,58337250.00,2,2,'2020-02-29 07:21:08','2020-02-29 07:27:06',NULL),(4,'INV-022920-3-1','2019-03-22',15501250,15501250.00,2,3,'2020-02-29 07:31:01','2020-02-29 07:35:17',NULL),(5,'INV-022920-3-5','2019-03-28',18225450,18225450.00,2,3,'2020-02-29 07:32:05','2020-02-29 07:36:34',NULL),(6,'INV-022920-4-1','2019-06-10',123031250,123031250.00,2,4,'2020-02-29 07:43:23','2020-02-29 07:57:28',NULL),(7,'INV-032920-5-1','2019-09-12',93436500,93436500.00,2,5,'2020-03-29 11:24:38','2020-03-29 11:49:44',NULL),(8,'INV-032920-5-8','2019-11-05',57393900,57393900.00,2,5,'2020-03-29 11:39:13','2020-03-29 11:54:23',NULL),(9,'INV-040520-3-6','2019-04-16',42690450,42690450.00,2,3,'2020-04-05 12:02:59','2020-04-05 12:08:57',NULL),(11,'INV-041420-12-1','2020-04-14',19000,0.00,0,12,'2020-04-14 09:05:40','2020-04-14 09:05:40',NULL),(12,'INV-041420-13-1','2020-04-14',247500,0.00,0,13,'2020-04-14 09:12:14','2020-04-14 09:12:14',NULL),(15,'INV-041420-12-15','2020-04-14',297500,0.00,0,12,'2020-04-14 09:20:39','2020-04-14 09:20:39',NULL),(16,'INV-041420-12-16','2020-04-14',2583500,0.00,0,12,'2020-04-14 09:21:40','2020-04-14 09:21:40',NULL);
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_permission`
--

DROP TABLE IF EXISTS `role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_permission` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_permission_role_id_foreign` (`role_id`),
  KEY `role_permission_permission_id_foreign` (`permission_id`),
  CONSTRAINT `role_permission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_permission`
--

LOCK TABLES `role_permission` WRITE;
/*!40000 ALTER TABLE `role_permission` DISABLE KEYS */;
INSERT INTO `role_permission` VALUES (1,2,1,NULL,NULL),(2,2,3,NULL,NULL),(3,2,2,NULL,NULL),(4,2,4,NULL,NULL),(5,2,5,NULL,NULL),(6,2,6,NULL,NULL),(7,2,7,NULL,NULL),(8,2,36,NULL,NULL),(9,2,38,NULL,NULL),(10,2,37,NULL,NULL),(11,2,39,NULL,NULL),(12,2,40,NULL,NULL),(13,2,41,NULL,NULL),(14,2,42,NULL,NULL),(15,1,8,NULL,NULL),(16,1,9,NULL,NULL),(17,1,10,NULL,NULL),(18,1,11,NULL,NULL),(19,1,12,NULL,NULL),(20,1,13,NULL,NULL),(21,1,14,NULL,NULL),(22,1,15,NULL,NULL),(23,1,16,NULL,NULL),(24,1,17,NULL,NULL),(25,1,18,NULL,NULL),(26,1,19,NULL,NULL),(27,1,20,NULL,NULL),(28,1,21,NULL,NULL),(29,1,22,NULL,NULL),(30,1,23,NULL,NULL),(31,1,24,NULL,NULL),(32,1,25,NULL,NULL),(33,1,26,NULL,NULL),(34,1,27,NULL,NULL),(35,1,28,NULL,NULL),(36,1,29,NULL,NULL),(37,1,31,NULL,NULL),(38,1,30,NULL,NULL),(39,1,32,NULL,NULL),(40,1,44,NULL,NULL),(41,1,45,NULL,NULL),(42,1,46,NULL,NULL),(43,1,50,NULL,NULL),(44,1,51,NULL,NULL),(45,1,52,NULL,NULL),(46,1,53,NULL,NULL),(47,3,8,NULL,NULL),(48,3,9,NULL,NULL),(49,3,15,NULL,NULL),(50,3,22,NULL,NULL),(51,3,10,NULL,NULL),(52,3,11,NULL,NULL),(53,3,12,NULL,NULL),(54,3,13,NULL,NULL),(55,3,14,NULL,NULL),(56,3,16,NULL,NULL),(57,3,17,NULL,NULL),(58,3,18,NULL,NULL),(59,3,19,NULL,NULL),(60,3,20,NULL,NULL),(61,3,21,NULL,NULL),(62,3,23,NULL,NULL),(63,3,24,NULL,NULL),(64,3,25,NULL,NULL),(65,3,26,NULL,NULL),(66,3,27,NULL,NULL),(67,3,28,NULL,NULL),(68,3,29,NULL,NULL),(69,3,30,NULL,NULL),(70,3,31,NULL,NULL),(71,3,32,NULL,NULL),(72,3,33,NULL,NULL),(73,3,34,NULL,NULL),(74,3,35,NULL,NULL),(75,3,43,NULL,NULL),(76,3,44,NULL,NULL),(77,3,45,NULL,NULL),(78,3,46,NULL,NULL),(79,3,47,NULL,NULL),(80,3,48,NULL,NULL),(81,3,49,NULL,NULL),(82,3,50,NULL,NULL),(83,3,51,NULL,NULL),(84,3,52,NULL,NULL),(85,3,53,NULL,NULL),(86,3,54,NULL,NULL),(87,3,55,NULL,NULL),(88,3,56,NULL,NULL),(89,3,57,NULL,NULL),(90,1,43,NULL,NULL);
/*!40000 ALTER TABLE `role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'staf',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(2,'admin',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29'),(3,'eksekutif',NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'UD. Sinar Cerah Sejahtera',NULL,NULL,'(0361) 421836','Jl. Bedahulu IV No.6, Peguyangan, Kec. Denpasar Utara',NULL,'2020-02-25 12:29:29','2020-02-25 12:46:44'),(2,'PT. Guna Elektro','info@gae.co.id',NULL,'(021) 5655010','Jl. Arjuna Utara 50 Jakarta Barat 11510 INDONESIA',NULL,'2020-02-25 12:29:29','2020-02-25 12:50:51'),(3,'PT. Prima Indah Lestari','info@extranacable.com','(021) 5550 559','(021) 5550861','Jl. Raya Tegal Alur No. 83 Jakarta Barat Jakarta Barat\nDKI Jakarta',NULL,'2020-02-25 12:29:29','2020-02-25 12:55:42'),(4,'CV. Citra Surya Dewata (CSDW)',NULL,NULL,'0877-6116-3939','Jl. Mahendradatta Denpasar Barat',NULL,'2020-02-25 12:29:29','2020-02-25 12:58:02'),(5,'UD. Mandiri',NULL,NULL,'(0361) 7804571','Jl. Jaya Giri Utara No.77, Panjer Denpasar Selatan',NULL,'2020-02-25 12:29:29','2020-02-25 12:59:32'),(6,'UD. Jasa Karya',NULL,NULL,'(0361) 420242','Jl Cokroaminoto No. 49 Denpasar Utara',NULL,'2020-02-25 12:29:29','2020-02-25 13:01:10'),(7,'UD. Purya Jaya',NULL,NULL,'(0361) 462971','Jl. Gatot Subroto Tim. No.150, Kesiman Petilan Denpasar Timur',NULL,'2020-02-25 12:29:29','2020-02-25 13:04:02'),(8,'Upton-Hamill','willms.claudine@rogahn.info','1-624-582-8098','+1.826.219.2345','711 London Burgs\nRosamouth, WY 60625-4883','2020-02-25 13:04:08','2020-02-25 12:29:29','2020-02-25 13:04:08'),(9,'Stanton LLC','fredrick.sipes@gmail.com','608.780.3344 x234','562-721-4836','82278 Brown Village Suite 645\nPort Barrett, CT 30639','2020-02-25 13:04:11','2020-02-25 12:29:29','2020-02-25 13:04:11'),(10,'Klocko Inc','nathaniel.langworth@yahoo.com','(529) 585-1295','+1-842-961-3894','8626 O\'Conner Port\nConnborough, WY 72639-4925','2020-02-25 13:04:14','2020-02-25 12:29:29','2020-02-25 13:04:14');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

<<<<<<< HEAD
LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'surya','staf@example.com','$2y$10$TB3mQeuqNa/VGN8zZhgffen6b1mI78veeM/bq2XLUArnD.4zts/Cu',1,NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(2,'admin','admin@example.com','$2y$12$RdPbXABKK.I05VI3FwcYK.AVk062VSRDmCqdoIqM9nbK1scx54NB2',2,NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(3,'ekesutif','eksekutif@example.com','$2y$10$Pt8/3UsD.SctM1n0LpWqOeq3TzCAKKZwJIEtIKoVikIze3Bxe8f1y',3,NULL,'2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(4,'Missouri Denesik','pbeer@example.org','$2y$10$ePu9CZklmBWcvG8BsUWqCO2t.z9aHz4s8eqKvAE1.nSqx1ik5dQy6',2,'4ZKizZrlUz','2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(5,'Dr. Layne Schiller MD','hilario.mueller@example.com','$2y$10$sIjyf.l5k3r/XH1RXMzQ8OUF6NFmOnViCYGcVw9TtxtexU92GAItW',2,'9aw8EgiTMq','2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(6,'Sydney Schultz','floy.doyle@example.net','$2y$10$2rKMQxBYPHw/yD.iJdysSeyvpEl82bq/FwdsTRBkBayZvbsZNgboi',3,'oLycTbBGMs','2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(7,'Catalina Bailey','oconnell.rogers@example.net','$2y$10$/lCGihkmmmRtvTrVxc.ub.YDClf9aFAbcF0rEy8.DPg12zdnSQ7dW',2,'HCvZunDlt2','2020-02-25 12:29:29','2020-02-25 12:29:29',NULL),(8,'Miss Eleanore Carter','becker.rosa@example.com','$2y$10$5N1yl/PXOeEdr7YMtNYNIuOxSLXgkkyEGxvRhjw9pwKfFBICd/jHi',2,'nIAsXfpZ9g','2020-02-25 12:29:29','2020-02-25 12:29:29',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;
=======
INSERT INTO `users` (`id`, `name`, `email`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'surya', 'staf@example.com', '$2y$10$TB3mQeuqNa/VGN8zZhgffen6b1mI78veeM/bq2XLUArnD.4zts/Cu', 1, NULL, '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(2, 'admin', 'admin@example.com', '$2y$12$RdPbXABKK.I05VI3FwcYK.AVk062VSRDmCqdoIqM9nbK1scx54NB2', 2, NULL, '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(3, 'ekesutif', 'eksekutif@example.com', '$2y$10$Pt8/3UsD.SctM1n0LpWqOeq3TzCAKKZwJIEtIKoVikIze3Bxe8f1y', 3, NULL, '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(4, 'Missouri Denesik', 'pbeer@example.org', '$2y$10$ePu9CZklmBWcvG8BsUWqCO2t.z9aHz4s8eqKvAE1.nSqx1ik5dQy6', 2, '4ZKizZrlUz', '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(5, 'Dr. Layne Schiller MD', 'hilario.mueller@example.com', '$2y$10$sIjyf.l5k3r/XH1RXMzQ8OUF6NFmOnViCYGcVw9TtxtexU92GAItW', 2, '9aw8EgiTMq', '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(6, 'Sydney Schultz', 'floy.doyle@example.net', '$2y$10$2rKMQxBYPHw/yD.iJdysSeyvpEl82bq/FwdsTRBkBayZvbsZNgboi', 3, 'oLycTbBGMs', '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(7, 'Catalina Bailey', 'oconnell.rogers@example.net', '$2y$10$/lCGihkmmmRtvTrVxc.ub.YDClf9aFAbcF0rEy8.DPg12zdnSQ7dW', 2, 'HCvZunDlt2', '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL),
(8, 'Miss Eleanore Carter', 'becker.rosa@example.com', '$2y$10$5N1yl/PXOeEdr7YMtNYNIuOxSLXgkkyEGxvRhjw9pwKfFBICd/jHi', 2, 'nIAsXfpZ9g', '2020-02-25 12:29:29', '2020-02-25 12:29:29', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `payments_payment_no_unique` (`payment_no`),
  ADD KEY `payments_project_id_foreign` (`project_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `projects_project_no_unique` (`project_no`),
  ADD KEY `projects_customer_id_foreign` (`customer_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchases_purchase_no_unique` (`purchase_no`),
  ADD KEY `purchases_project_id_foreign` (`project_id`);

--
-- Indexes for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_details_item_id_foreign` (`item_id`),
  ADD KEY `purchase_details_purchase_id_foreign` (`purchase_id`);

--
-- Indexes for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `purchase_payments_payment_no_unique` (`payment_no`),
  ADD KEY `purchase_payments_purchase_id_foreign` (`purchase_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_permission_role_id_foreign` (`role_id`),
  ADD KEY `role_permission_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `purchases`
--
ALTER TABLE `purchases`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `purchase_details`
--
ALTER TABLE `purchase_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`);

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_project_id_foreign` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_details`
--
ALTER TABLE `purchase_details`
  ADD CONSTRAINT `purchase_details_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`),
  ADD CONSTRAINT `purchase_details_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `purchase_payments`
--
ALTER TABLE `purchase_payments`
  ADD CONSTRAINT `purchase_payments_purchase_id_foreign` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`id`);

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
  ADD CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;
>>>>>>> 01f14d6d6c34e1da5a97a39cd702f857ddab1b4a

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-04-14 17:33:57
