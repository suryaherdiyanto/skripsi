<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staf = App\Role::create([
            'name' => 'staf'
        ]);

        $admin = App\Role::create([
            'name' => 'admin'
        ]);

        $executive = App\Role::create([
            'name' => 'eksekutif'
        ]);

        $admin->givePermissions([
            'list user',
            'store user',
            'show user',
            'update user',
            'delete user',
            'force-delete user',
            'restore user',
            'list role',
            'store role',
            'show role',
            'update role',
            'delete role',
            'force-delete role',
            'restore role',
        ]);

        $staf->users()->create([
            'name' => 'surya',
            'email' => 'staf@example.com',
            'password' => '123123'
        ]);

        $admin->users()->create([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => '123123'
        ]);

        $executive->users()->create([
            'name' => 'ekesutif',
            'email' => 'eksekutif@example.com',
            'password' => '123123'
        ]);
    }
}
