<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = [
            'user',
            'item',
            'customer',
            'supplier',
            'project',
            'role',
            'purchase',
            'payment'
        ];

        $actions = [
            'list',
            'show',
            'store',
            'update',
            'delete',
            'force-delete',
            'restore'
        ];

        $permissions = $this->combine($modules, $actions);

        foreach ($permissions as $key => $value) {
            Permission::create(['name' => $value]);
        }
    }

    private function combine(array $modules, array $actions)
    {
        $data = [];
        foreach ($modules as $key => $value) {
    
            foreach ($actions as $key_action => $value_action) {
                $data[] = $value_action . ' ' . $value;
            }

        }

        return $data;
    }
}
