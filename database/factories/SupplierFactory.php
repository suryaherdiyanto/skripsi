<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Supplier;
use Faker\Generator as Faker;

$factory->define(Supplier::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'email' => $faker->email,
        'fax' => $faker->phoneNumber,
        'phone' => $faker->phoneNumber,
        'address' => $faker->address
    ];
});
