<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Payment;
use App\Project;
use Faker\Generator as Faker;

$factory->define(Payment::class, function (Faker $faker) {
    $project = Project::all()->random();
    return [
        'payment_no' => getPaymentNo($project),
        'date' => $faker->date(),
        'total' => $faker->randomFloat(2, 50000, 5000000),
        'project_id' => $project->id
    ];
});
