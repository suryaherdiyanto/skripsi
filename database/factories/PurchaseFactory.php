<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Purchase;
use Faker\Generator as Faker;

$factory->define(Purchase::class, function (Faker $faker) {
    $project = App\Project::all()->random();
    return [
        'purchase_no' => getPurchaseNo($project),
        'date' => $faker->date(),
        'total' => $faker->randomFloat(2, 50000, 5000000),
        'project_id' => $project->id
    ];
});
