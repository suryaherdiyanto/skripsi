<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        'project_no' => getProjectNo(),
        'start_date' => $faker->date(),
        'total_cost' => 0,
        'total_paid' => 0,
        'description' => $faker->paragraphs(3, true),
        'customer_id' => App\Customer::all()->random()->id
    ];
});
