<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;

$factory->define(Item::class, function (Faker $faker) {
    return [
        'code' => getItemCode(),
        'name' => $faker->words(2, true),
        'sell_price' => $faker->numberBetween(5000, 50000),
        'purchase_price' => 0,
        'qty_type' => $faker->randomElement(['pcs', 'box', 'roll']),
        'description' => $faker->paragraphs(2, true),
        'supplier_id' => \App\Supplier::latest()->first()->id
    ];
});
