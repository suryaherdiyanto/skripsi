<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\PurchasePayment;
use App\Purchase;

$factory->define(PurchasePayment::class, function (Faker $faker) {
    $purchase  = Purchase::all()->random();
    return [
        'payment_no' => getPaymentNo($purchase),
        'date' => $faker->date(),
        'total' => $faker->randomFloat(2, 50000, 5000000),
        'purchase_id' => $purchase->id
    ];
});
