<?php

use Illuminate\Http\Request;

$api = app('Dingo\Api\Routing\Router');

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api->version('v1', function() use($api) {

    $api->group(['namespace' => 'App\Http\Controllers\Api'], function() use($api) {
        $api->post('login', 'LoginController@login');
        
        $api->group(['middleware' => ['jwt.auth', 'bindings']], function() use($api) {
            $api->get('me', 'LoginController@me');
            $api->post('me/reset-password', 'LoginController@passwordReset');
            $api->get('me/permission', 'LoginController@getPermissions');
            $api->post('logout', 'LoginController@logout');
            
            $api->get('users', 'UserController@index');
            $api->post('users', 'UserController@store');
            $api->get('users/{id}', 'UserController@show');
            $api->put('users/{user}', 'UserController@update');
            $api->delete('users/{user}', 'UserController@destroy');
            $api->delete('users/{user}/force-delete', 'UserController@forceDelete');
            $api->delete('users/trash/clear', 'UserController@clearTrash');
            $api->put('users/{user}/restore', 'UserController@restore');
    
            $api->get('permissions', 'PermissionController@index');
            $api->post('permissions', 'PermissionController@store');
            $api->get('permissions/{permission}', 'PermissionController@show');
            $api->put('permissions/{permission}', 'PermissionController@update');
            $api->delete('permissions/{permission}', 'PermissionController@destroy');
    
            $api->get('roles', 'RoleController@index');
            $api->post('roles', 'RoleController@store');
            $api->get('roles/{role}', 'RoleController@show');
            $api->put('roles/{role}', 'RoleController@update');
            $api->delete('roles/{role}', 'RoleController@destroy');
            $api->delete('roles/{role}/force-delete', 'RoleController@forceDelete');
            $api->delete('roles/trash/clear', 'RoleController@clearTrash');
            $api->put('roles/{role}/restore', 'RoleController@restore');
    
            $api->get('suppliers', 'SupplierController@index');
            $api->post('suppliers', 'SupplierController@store');
            $api->get('suppliers/{supplier}', 'SupplierController@show');
            $api->put('suppliers/{supplier}', 'SupplierController@update');
            $api->delete('suppliers/{supplier}', 'SupplierController@destroy');
            $api->delete('suppliers/{supplier}/force-delete', 'SupplierController@forceDelete');
            $api->delete('suppliers/trash/clear', 'SupplierController@clearTrash');
            $api->put('suppliers/{supplier}/restore', 'SupplierController@restore');
    
            $api->get('items', 'ItemController@index');
            $api->post('items', 'ItemController@store');
            $api->get('items/{item}', 'ItemController@show');
            $api->put('items/{item}', 'ItemController@update');
            $api->delete('items/{item}', 'ItemController@destroy');
            $api->delete('items/{item}/force-delete', 'ItemController@forceDelete');
            $api->delete('items/trash/clear', 'ItemController@clearTrash');
            $api->put('items/{item}/restore', 'ItemController@restore');
    
            $api->get('customers', 'CustomerController@index');
            $api->post('customers', 'CustomerController@store');
            $api->get('customers/{customer}', 'CustomerController@show');
            $api->put('customers/{customer}', 'CustomerController@update');
            $api->delete('customers/{customer}', 'CustomerController@destroy');
            $api->delete('customers/{supplier}/force-delete', 'CustomerController@forceDelete');
            $api->delete('customers/trash/clear', 'CustomerController@clearTrash');
            $api->put('customers/{supplier}/restore', 'CustomerController@restore');
    
            $api->get('projects', 'ProjectController@index');
            $api->post('projects', 'ProjectController@store');
            $api->get('projects/{project}', 'ProjectController@show');
            $api->put('projects/{project}', 'ProjectController@update');
            $api->delete('projects/{project}', 'ProjectController@destroy');
            $api->delete('projects/{project}/force-delete', 'ProjectController@forceDelete');
            $api->delete('projects/trash/clear', 'ProjectController@clearTrash');
            $api->put('projects/{project}/restore', 'ProjectController@restore');

            $api->group(['prefix' => 'reports'], function() use($api) {
                $api->get('/project', 'ReportController@projectReport');
                $api->get('/profit', 'ReportController@profitReport');
                $api->get('/purchase', 'ReportController@purchaseReport');
                $api->get('/payment', 'ReportController@paymentReport');
                $api->get('/dashboard', 'ReportController@incomeOutcomeReport');
                $api->get('/totals', 'ReportController@total');
            });

            $api->group(['prefix' => 'purchase'], function() use($api) {
                $api->get('/{purchase}/payments', 'PurchasePaymentController@index');
                $api->post('/{purchase}/payments', 'PurchasePaymentController@store');
                $api->get('/payments/{payment}', 'PurchasePaymentController@show');
                $api->put('/payments/{payment}', 'PurchasePaymentController@update');
                $api->delete('/payments/{payment}', 'PurchasePaymentController@destroy');
                $api->delete('/payments/trash/clear', 'PurchasePaymentController@clearTrash');

                $api->get('/{purchase}/details', 'PurchaseDetailController@index');
                $api->post('/{purchase}/details', 'PurchaseDetailController@store');
                $api->get('/details/{detail}', 'PurchaseDetailController@show');
                $api->put('/details/{detail}', 'PurchaseDetailController@update');
                $api->put('/details/{detail}/restore', 'PurchaseDetailController@restore');
                $api->delete('/details/{detail}', 'PurchaseDetailController@destroy');
                $api->delete('/details/{detail}/force-delete', 'PurchaseDetailController@forceDelete');
                $api->delete('/details/trash/clear', 'PurchaseDetailController@clearTrash');
            });
    
            $api->group(['prefix' => 'projects'], function() use($api) {
    
                $api->get('/{project}/purchases', 'PurchaseController@index');
                $api->post('/{project}/purchases', 'PurchaseController@store');
                $api->get('purchases/{purchase}', 'PurchaseController@show');
                $api->put('purchases/{purchase}', 'PurchaseController@update');
                $api->delete('purchases/{purchase}', 'PurchaseController@destroy');
                $api->put('purchases/{purchase}/restore', 'PurchaseController@restore');
                $api->delete('purchases/{purchase}/force-delete', 'PurchaseController@forceDelete');
                $api->delete('purchases/trash/clear', 'PurchaseController@clearTrash');

                $api->get('/{project}/payments', 'PaymentController@index');
                $api->post('/{project}/payments', 'PaymentController@store');
                $api->get('payments/{payment}', 'PaymentController@show');
                $api->put('payments/{payment}', 'PaymentController@update');
                $api->delete('payments/{payment}', 'PaymentController@destroy');
                $api->put('payments/{payment}/restore', 'PaymentController@restore');
                $api->delete('payments/{payment}/force-delete', 'PaymentController@forceDelete');
                $api->delete('payments/trash/clear', 'PaymentController@clearTrash');

    
            });
        });
        


    });

});



